<?
$MESS["PYDELIVERY_INSTALL_TITLE"] = "Установка YDELIVERY";
$MESS["PYDELIVERY_MAIN_MODULE_NAME"] = "Яндекс Доставка";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_COMMON_INFO"] = "Общие сведения";
$MESS["PYDELIVERY_MAIN_MODULE_KEY_ERRORS"] = "Не верные ключи/Не удалось соединться с Яндекс сервером";
$MESS["PYDELIVERY_MAIN_MODULE_CITY_ERROR"] = "Не определён город доставки или город отправления";
$MESS["PYDELIVERY_MAIN_MODULE_DATAPROP_ERROR"] = "Не определено свойство для данных Яндекс.Доставки";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_COMMON_INFO_AND_SETTINGS"] = "Общие сведения и настройки";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_SETTINGS"] = "Настройки модуля YDELIVERY";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_LOCAL_SETTINGS"] = "Настройки модуля YDELIVERY";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_PYDELIVERY_SETTINGS"] = "Настройка параметров интеграции с Яндекс Доставкой";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_TAB_ORDERS"] = "Управление заказами";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_TAB_COMMON_INFO"] = "Данный модуль предназначен для интеграции интернет магазина с <a href='https://delivery.yandex.ru/' target='_blank'>Яндекс доставкой</a>. (версия битрикс: не ниже 16.0.0)";
$MESS["PYDELIVERY_MAIN_MODULE_CURL_INFO"] = "Для работы модуля требуется установленное расширение для php &laquo;php_curl.dll&raquo;<br>Текущее состояние:";
$MESS["PYDELIVERY_MAIN_MODULE_CURL_ON"] = "php_curl.dll включен";
$MESS["PYDELIVERY_MAIN_MODULE_CURL_OFF"] = "php_curl.dll не доступен! Установите расширение extension=php_curl.dll";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_TAB_API_KEYS"] = "Для получения ключей интеграции <br> вам нужно заключить договор с Компанией Яндекс доставка.";
$MESS["PYDELIVERY_MAIN_MODULE_OPTION_VALS"] = "Option values";
$MESS["PYDELIVERY_MAIN_MODULE_METHOD_KEYS"] = "Method Keys";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_YDDATA"] = "Код свойства для Данных ЯндексДоставки";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_CITY"] = "Код свойства для города";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_STREET"] = "Код свойства для улицы";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_HOUSE"] = "Код свойства для номера дома";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_INDEX"] = "Код свойства для индекса";
$MESS["PYDELIVERY_MAIN_MODULE_EXPORT_INFO"] = "Данные для выгрузки заказа";
$MESS["PYDELIVERY_MAIN_MODULE_SALEORDER_INFO"] = "Данные для страницы оформления заказа";
$MESS["PYDELIVERY_MAIN_MODULE_GROUP_PP"] = "Группировать пункты самовывоза в сайдбаре";
$MESS["PYDELIVERY_MAIN_MODULE_SHOW_ERROR_PP"] = "Выводить ошибку &laquo;Не выбран вариант доставки&raquo;";
$MESS["PYDELIVERY_MAIN_MODULE_PP_NOT_SELECTED"] = "Не выбран вариант доставки";
$MESS["PYDELIVERY_MAIN_MODULE_AUTOEXPORT"] = "Автоматическая выгрузка заказов";
$MESS["PYDELIVERY_MAIN_MODULE_EXPORTONLYPAYED"] = "Автоматически выгружать только оплаченные заказы";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_NAME"] = "Код свойства для имени получателя";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_MIDNAME"] = "Код свойства для отчества получателя";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_LASTNAME"] = "Код свойства для фамилии получателя";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_PHONE"] = "Код свойства для телефона получателя";
$MESS["PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_EMAIL"] = "Код свойства для Email-а получателя";
$MESS["PYDELIVERY_MAIN_MODULE_RAND_DIMENSIONS"] = "Размеры товара по умолчанию (см)";
$MESS["PYDELIVERY_MAIN_MODULE_RAND_WEIGHT"] = "Вес товара по умолчанию (грамм)";
$MESS["PYDELIVERY_MAIN_MODULE_SHOP_CITY"] = "Город магазина (город отправления)";
$MESS["PYDELIVERY_MAIN_MODULE_RAND_CITY"] = "Москва";
$MESS["PYDELIVERY_MAIN_MODULE_NOTSELECTED"] = "не выбрано";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER"] = "Заказ";
$MESS["PYDELIVERY_MAIN_MODULE_ORDERS"] = "Заказы";
$MESS["PYDELIVERY_MAIN_MODULE_USER"] = "Пользователь";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DATE"] = "Дата заказа";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_SUMM"] = "Сумма";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_STATUS"] = "Статус";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_GOODS"] = "Товары";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_EXPORT"] = "Выгрузить в ЯндексДоставку";
$MESS["PYDELIVERY_MAIN_MODULE_FOOT_ORDERS"] = "фЗаказы";
$MESS["PYDELIVERY_MAIN_MODULE_T_HEADER"] = "Заголовок 2";
$MESS["PYDELIVERY_MAIN_MODULE_PAGE_ERROR"] = "Ошибка";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_WEIGHT"] = "Вес Заказа(кг)";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DIMENSIONS"] = "Размеры заказа (см)";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_COMMENT"] = "Комментарий к заказу";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_PRICE"] = "Оценочная стоимость заказа";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_PREPAID"] = "100% Предоплата по заказу";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_COST"] = "Стоимость доставки";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_USER_NAME"] = "Имя получателя";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_USER_SECOND_NAME"] = "Отчество получателя";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_USER_LAST_NAME"] = "Фамилия получателя";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_USER_PHONE"] = "Телефон получателя";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_USER_EMAIL"] = "Email получателя";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_CITY"] = "Город доставки";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_STREET"] = "Название улицы";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_HOUSE"] = "Номер дома";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_FLOOR"] = "Номер квартиры";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_HOUSING"] = "Номер корпуса";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_INDEX"] = "Почтовый индекс";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_TO_YD_WAREHOUSE"] = "Склад, на который осуществляется отгрузка";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_TO_YD_WAREHOUSE_0"] = "склад службы доставки";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_TO_YD_WAREHOUSE_1"] = "единый склад";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_EXPORTED_DATA"] = "Данные для выгрузки";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_ALL_PROPS"] = "Свойства заказа";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_EXPORT_SUBMIT"] = "Подтвердить выгрузку!";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_GOODS_DATA"] = "Данные товаров";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_GOODS_ARTICLE"] = "Артикул";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_GOODS_NAME"] = "Наименование";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_GOODS_QUANTITY"] = "Количество";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_GOODS_COST"] = "Цена";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_GOODS_VAT"] = "НДС";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_EXPORT_SUCCESS"] = "Заказ выгружен успешно!";
$MESS["PYDELIVERY_MAIN_MODULE_ERROR_NOT_CITY"] = "Не выбран город доставки!";
$MESS["PYDELIVERY_MAIN_MODULE_ERROR_NOT_DELIVERY"] = "Не выбрана служба доставки!";
$MESS["PYDELIVERY_MAIN_MODULE_ERROR_NOT_YDDATA"] = "Не выбрано свойство, для данных ЯндексДоставки!";
$MESS["PYDELIVERY_MAIN_MODULE_ERROR_NO_SHOP_CITY"] = "Настройте город отправки и код города доставки в настройках модуля ЯндексДоставки";
$MESS["PYDELIVERY_MAIN_MODULE_ERROR_NO_OPTION_KEYS"] = "Установите \"Option values\" и \"Method Keys\" в настройках модуля \"Служба доставки Яндекс Доставка\"";
$MESS["PYDELIVERY_MAIN_MODULE_ERROR_MODULE"] = "Ошибка подключения модуля ЯндексДоставки!";
$MESS["PYDELIVERY_MAIN_MODULE_SELECT_VAR"] = "Изменить службу доставки →";
$MESS["PYDELIVERY_MAIN_MODULE_TYPE_PICKUP"] = "Самовывоз";
$MESS["PYDELIVERY_MAIN_MODULE_TYPE_TODOOR"] = "Курьер";
$MESS["PYDELIVERY_MAIN_MODULE_TYPE_POST"] = "Почта";
$MESS["PYDELIVERY_MAIN_MODULE_PHONE"] = "тел:";
$MESS["PYDELIVERY_MAIN_MODULE_ADDRESS"] = "Адрес:";
$MESS["PYDELIVERY_MAIN_MODULE_POINT"] = "Точка:";
$MESS["PYDELIVERY_MAIN_MODULE_DAY1"] = "день";
$MESS["PYDELIVERY_MAIN_MODULE_DAY2"] = "дня";
$MESS["PYDELIVERY_MAIN_MODULE_DAY3"] = "дней";
$MESS["PYDELIVERY_MAIN_HANDLER_SETTINGS"] = "Настройка обработчика";
$MESS["PYDELIVERY_MAIN_ORDER_ADD_PRICE"] = "Надбавка к рассчитанной стоимости (в рублях)";
$MESS["PYDELIVERY_MAIN_MODULE_TARIF"] = "тариф";
$MESS["PYDELIVERY_MAIN_MODULE_MONDAY"] = "пн";
$MESS["PYDELIVERY_MAIN_MODULE_TUESDAY"] = "вт";
$MESS["PYDELIVERY_MAIN_MODULE_WEDNESDAY"] = "ср";
$MESS["PYDELIVERY_MAIN_MODULE_THURSDAY"] = "чт";
$MESS["PYDELIVERY_MAIN_MODULE_FRIDAY"] = "пт";
$MESS["PYDELIVERY_MAIN_MODULE_SATURDAY"] = "сб";
$MESS["PYDELIVERY_MAIN_MODULE_SUNDAY"] = "вс";
$MESS["PYDELIVERY_MAIN_MODULE_DAY"] = "день";
$MESS["PYDELIVERY_MAIN_MODULE_DAY_IN"] = "дня";
$MESS["PYDELIVERY_MAIN_MODULE_DAYS"] = "дней";
$MESS["PYDELIVERY_MAIN_MODULE_LIMIT"] = "срок";
$MESS["PYDELIVERY_MAIN_MODULE_DELIVERY_DAYS"] = "Даты доставки";
$MESS["PYDELIVERY_MAIN_MODULE_SELECT"] = "Выбрать";
$MESS["PYDELIVERY_MAIN_MODULE_POINT"] = "Точка";
$MESS["PYDELIVERY_MAIN_MODULE_POSSIBLE_DATES"] = "Даты, в которые возможна доставка в пункт самовывоза:";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_"] = "";
$MESS["PYDELIVERY_MAIN_MODULE_ORDER_"] = "";
?>