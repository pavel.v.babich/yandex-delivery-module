<?
IncludeModuleLangFile(__FILE__, "ru");

require("classes/general/ModuleMain.php");
require("classes/general/EventsHandel.php");
require("classes/general/MainSettings.php");

if(false)CModule::AddAutoloadClasses(
    "pavelbabich.ydelivery",
    array(
        "YdeliveryModuleMain" => "/classes/general/ModuleMain.php",
        "YdeliveryMainSettings" => "/classes/general/MainSettings.php",
        "YdeliveryEventsHandel" => "/classes/general/EventsHandel.php"
    )
);

// \Bitrix\Main\Loader::registerAutoLoadClasses("iblock", $arClasses);
?>