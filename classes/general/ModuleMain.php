<?
namespace PYDELIVERYMain;

use CSaleOrderProps,
    CSalePersonType,
    CIBlockElement,
    CCatalogSku,
    \Bitrix\Main\Data\Cache,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Context,
    Bitrix\Main\Loader,
    Bitrix\Sale,
    Bitrix\Main\UserTable;

// Documentation: https://tech.yandex.ru/delivery/doc/dg/reference/search-delivery-list-docpage/
class PYDELIVERYModuleMain{
    
    protected static $module = "pavelbabich.ydelivery";
    public $status;
    private $options;
    private $arUrls = [
            "autocomplete" => "https://delivery.yandex.ru/api/last/autocomplete",
            "getIndex" => "https://delivery.yandex.ru/api/last/getIndex",
            "getDeliveries" => "https://delivery.yandex.ru/api/last/getDeliveries",
            "searchDeliveryList" => "https://delivery.yandex.ru/api/last/searchDeliveryList",
            "createOrder" => "https://delivery.yandex.ru/api/last/createOrder",
            "confirmSenderOrders" => "https://delivery.yandex.ru/api/last/confirmSenderOrders",
            "getOrderInfo" => "https://delivery.yandex.ru/api/last/getOrderInfo",
            "getSenderOrders" => "https://delivery.yandex.ru/api/last/getSenderOrders",
            "getSenderOrderStatus" => "https://delivery.yandex.ru/api/last/getSenderOrderStatus",
            "deleteOrder" => "https://delivery.yandex.ru/api/last/deleteOrder",
            "updateOrder" => "https://delivery.yandex.ru/api/last/updateOrder",
            "searchSenderOrdersStatuses" => "https://delivery.yandex.ru/api/last/searchSenderOrdersStatuses",
        ];

    function __construct($JSONkeys=false, $JSONopt=false){
        if(!$JSONopt)$JSONopt = Option::get(static::$module, "OPTIONS_KEY");
        if(!$JSONkeys)$JSONkeys = Option::get(static::$module, "METHOD_KEYS");
        if(!$JSONopt || !$JSONkeys){
          $this->status = "ERROR";
          return false;
        }
        if(mb_strtolower(LANG_CHARSET)=="windows-1251")$JSONopt = iconv('windows-1251','utf-8',$JSONopt);
        $arKeys = json_decode($JSONkeys,true);
        $arOpt = json_decode($JSONopt,true);
        if(!is_array($arKeys) || !is_array($arOpt)){
          //trigger_error('The class ' . get_called_class() . ' get invalid parameters.', E_USER_ERROR);          
          $this->status = "ERROR";
          return false;
        }
        
        $this->options = [
            "METHOD_KEYS" => $arKeys,
            "IDENTITIES" => $arOpt
        ];
        $this->status = "READY";
        return true;
    }
    
    public function cURL($url, $p){
        $ch =  curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);    
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        // curl_setopt($ch, CURLOPT_REFERER, 'http://'.$_SERVER["SERVER_NAME"].'/');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);    
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
        if($p){
          curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($p));
        }
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if ($result){
            return array("RESULT" => $result, "HEADERS" => $info);
        }else{
            return '';
        }
    }

    public function getPostValues($data){
        if (!is_array($data)) return $data;    
        ksort($data);    
        return join('', array_map(function($k)
                {
                  return $this->getPostValues($k);
                }, 
            $data));
    }
    
    public function autocomplete($term, $type = "locality", $locality_name = false){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'term' => $term,
                'type' => $type
            ];
        if($locality_name)$arData["locality_name"] = $locality_name;
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        
        return json_decode($res["RESULT"],true);
    }
    
    public function getIndex($address){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'address' => $address,
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }
    
    public function getDeliveries($type = "fulfillment"){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'type' => $type
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        
        return json_decode($res["RESULT"],true);
    }
    
    public function searchDeliveryList($city_to, $delivery_type = "todoor", $INDEX = false){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $CITY_FROM = Option::get(static::$module, "SHOP_CITY");
        $ArBasket = $this->getYDBasket(false);
        $rDim = ceil(pow($ArBasket["VOLUME"],1/3));
        if(mb_strtolower(LANG_CHARSET)=="windows-1251") $CITY_FROM = iconv('windows-1251','utf-8',$CITY_FROM);
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'city_from' => $CITY_FROM,
                'city_to' => $city_to,
                // 'delivery_type' => $delivery_type, // Yandex unworking parameter
                'index_city' => $INDEX ? "" : "",
                'weight' => $ArBasket["WEIGHT"]/1000,
                'length' => $rDim,
                'width' => $rDim,
                'height' => $rDim,
                'total_cost' => $ArBasket["PRICE"]
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        
        $CACHE_KEY = md5("YD_VAR_".serialize($arData));
        $cache = Cache::createInstance();
        if($cache->initCache(7200, $CACHE_KEY)){
            $result = $cache->getVars();
        }elseif ($cache->startDataCache()) {
            $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
            $result = json_decode($res["RESULT"],true);
            $cache->endDataCache($result);
        }
        if($delivery_type == "post" || $delivery_type == "pickup"){ // Delete expensive post, pickup variants
            $arPickupIDs = [];
            foreach($result["data"] as $key => $dService){
                if(strtolower($dService["type"])!=$delivery_type)continue;
                foreach($dService["pickupPoints"] as $pickupPoint){
                    if($arPickupIDs[$pickupPoint["id"]]){
                        if($arPickupIDs[$pickupPoint["id"]]["COST"]>$dService["costWithRules"]){
                            unset($result["data"][$arPickupIDs[$pickupPoint["id"]]["KEY"]]);
                        }else{
                            unset($result["data"][$key]); continue;
                        }
                    }
                    $arPickupIDs[$pickupPoint["id"]] = ["KEY" => $key, "COST"=> $dService["costWithRules"]];
                }
            }
        }
        return $result;
    }
    
    public function getYDBasket($orderID = false){
        $randLength = intval(Option::get(static::$module, "RAND_LENGTH"));
        if(!$randLength)$randLength = 10;
        $randWidth = intval(Option::get(static::$module, "RAND_WIDTH"));
        if(!$randWidth)$randWidth = 10;
        $randHeight = intval(Option::get(static::$module, "RAND_HEIGHT"));
        if(!$randHeight)$randHeight = 10;
        $randWeight = intval(Option::get(static::$module, "RAND_WEIGHT"));
        if(!$randWeight)$randWeight = 1;
        
        $arBasket = [];
        if(Loader::includeModule("sale")){
            if($orderID){
                $order = Sale\Order::load($orderID);
                $basket = $order->getBasket();
            }else{
                $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Context::getCurrent()->getSite());
            }
            foreach($basket as $item){
                $BItem = [
                    "ID" => $item->getField('ID'),
                    "NAME" => $item->getField('NAME'),
                    "PRODUCT_ID" => $item->getProductId(),
                    "LENGHT" => intval($item->getField('LENGHT'))?intval($item->getField('LENGHT')):$randHeight,
                    "WIDTH" => intval($item->getField('WIDTH'))?intval($item->getField('WIDTH')):$randHeight,
                    "HEIGHT" => intval($item->getField('HEIGHT'))?intval($item->getField('HEIGHT')):$randHeight,
                    "WEIGHT" => intval($item->getWeight())?intval($item->getWeight()):$randWeight,
                    "QUANTITY" => $item->getQuantity(),
                    "PRICE" => round($item->getPrice()),
                    "VAT" => $item->getField('VAT_RATE')*100
                ];
                $arBasket["ITEMS"][] = $BItem;
                $arBasket["WEIGHT"] += $BItem["QUANTITY"]*$BItem["WEIGHT"];
                $arBasket["PRICE"] += $BItem["QUANTITY"]*$BItem["PRICE"];
                $arBasket["VOLUME"] += $BItem["LENGHT"]*$BItem["WIDTH"]*$BItem["HEIGHT"];
            }
        }
        return $arBasket;
    }
    
    public function getOrderPropsCodeAssoc(){
        $result = [];
        if(Loader::includeModule("sale")){
            $rsProps = CSaleOrderProps::GetList(["SORT" => "ASC"], ["ACTIVE"=>"Y"], false, false, []);
            while($arProp = $rsProps->Fetch()){
                if(!in_array($arProp["TYPE"],["TEXT", "LOCATION"]))continue;
                $result[$arProp["CODE"]][$arProp["ID"]] = [
                    "ID" => $arProp["ID"],
                    "NAME" => $arProp["NAME"],
                    "CODE" => $arProp["CODE"],
                    "PERSON_TYPE_ID" => $arProp["PERSON_TYPE_ID"],
                    "IS_LOCATION" => $arProp["IS_LOCATION"],
                    "IS_ZIP" => $arProp["IS_ZIP"]
                ];
            }
        }
        return $result;
    }
    
    public function getPersonTypes(){
        $result = [];
        if(Loader::includeModule("sale")){
            $rsPersonTypes = CSalePersonType::GetList(["SORT" => "ASC"], ["ACTIVE"=>"Y"], false, false, []);
            while($arPersonType = $rsPersonTypes->Fetch()){
                $result[$arPersonType["ID"]] = [
                    "ID" => $arPersonType["ID"],
                    "NAME" => $arPersonType["NAME"]
                  ];
            }
        }
        return $result;
    }
    
    public function getCoords($address){
        include("citybase.php");
        $CACHE_KEY = md5("YD_VAR_".$address);
        $cache = Cache::createInstance();
        if($citybase[mb_strtolower($address)]){
          $arCity = $citybase[mb_strtolower($address)];
          $result = ["LAT"=>$arCity["lat"],"LNG"=>$arCity["lng"]];
        }elseif($cache->initCache(720000, $CACHE_KEY)){
            $result = $cache->getVars();
        }elseif ($cache->startDataCache()) {
            $geolocation_json = $this->cURL('https://geocode-maps.yandex.ru/1.x/?format=json&geocode='.urlencode($address), false);
            $arrGeoLocation = json_decode($geolocation_json["RESULT"]);
            $coordinates = $arrGeoLocation->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
            if($coordinates){
                $arrCoord = explode(" ", $coordinates);
                $result = ["LAT"=>$arrCoord[1],"LNG"=>$arrCoord[0]];
                $cache->endDataCache($result);
            }
            if(!$result["LAT"] || !$result["LNG"]){
                $result = ["LAT"=>"55.753215","LNG"=>"37.622504"];
            }
        }
        return $result;
    }
    
    public function createOrder($DataOrder){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $DataOrder["client_id"] = $this->options["IDENTITIES"]["client"]["id"];
        $DataOrder["sender_id"] = $this->options["IDENTITIES"]["senders"][0]["id"];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $DataOrder['secret_key'] = md5($this->getPostValues($DataOrder) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $DataOrder);
        $ExportResult = json_decode($res["RESULT"],true);
        $EXP_ID = $ExportResult["data"]["order"]["id"];
        $FULL_NUM = $ExportResult["data"]["order"]["full_num"];
        if($EXP_ID && Loader::includeModule("sale")){
            $obOrder = Sale\Order::load($DataOrder["order_num"]);
            $propertyCollection = $obOrder->getPropertyCollection();
            $arPropVals = $propertyCollection->getArray();
            $PROP_CODE_YDDATA = Option::get(static::$module, "NAMEPROP_YDDATA");
            foreach($arPropVals["properties"] as $prop){
                if($prop["CODE"] == $PROP_CODE_YDDATA){
                    $YDDataPropID = $prop["ID"];
                    $YDDataProp = $propertyCollection->getItemByOrderPropertyId($YDDataPropID);
                    if($YDDataProp)$YDDataPropValue = $YDDataProp->getValue();
                    if(substr_count($YDDataPropValue, "YDORDER_ID")==0){
                        $YDDataPropValue .= " | YDORDER_ID:".$EXP_ID." | TREK_NUM:".$FULL_NUM;
                        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/ExportResultY.log", date("d-m-Y H:i:s")."SET; YDDataPropID=".$YDDataPropID."; YDDataPropValue=".$YDDataPropValue.";\n", FILE_APPEND);
                        $YDDataProp->setValue($YDDataPropValue);
                        $YDDataProp->save();
                        $ExportResult["YDDataPropID"] = $YDDataPropID;
                        $ExportResult["YDDataPropValue"] = $YDDataPropValue;
                    }
                    break;
                }
            }
        }
        return $ExportResult;
    }
    
    public function confirmSenderOrders($order_ids){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'order_ids' => $order_ids,
                'shipment_date' => date("Y-m-d"),
                'type' => "import"  // withdraw � �����; import � ����������.
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }
    
    public function getOrderInfo($order_id){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'order_id' => $order_id
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }
    
    public function getSenderOrders($order_ids){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'order_ids' => $order_ids,
                // 'deliveries' => "",
                // 'shops' => "",
                // 'statuses' => "",
                // 'page' => ""
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }

    public function getSenderOrderStatus($order_id){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'order_id' => $order_id
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }

    public function searchSenderOrdersStatuses($orders_id="", $date_from=false){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'orders_id' => $order_id,
                'transition_date_from' => $date_from
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }

    public function GetOrderData($orderId){
        $order = Sale\Order::load($orderId);
        $propertyCollection = $order->getPropertyCollection();
        $arPropVals = $propertyCollection->getArray();
        $BASKET = $this->getYDBasket($orderId);
        $rDim = ceil(pow($BASKET["VOLUME"],1/3));
        $arUser = UserTable::GetByID($order->getUserId())->Fetch();
        $PROP_CODE_USERNAME = Option::get(static::$module, "NAMEPROP_USER_NAME");
        $PROP_CODE_MIDLNAME = Option::get(static::$module, "NAMEPROP_USER_MIDNAME");
        $PROP_CODE_LASTNAME = Option::get(static::$module, "NAMEPROP_USER_LASTNAME");
        $PROP_CODE_USERPHONE = Option::get(static::$module, "NAMEPROP_USER_PHONE");
        $PROP_CODE_USEREMAIL = Option::get(static::$module, "NAMEPROP_USER_EMAIL");
        $PROP_CODE_YDDATA = Option::get(static::$module, "NAMEPROP_YDDATA");
        $PROP_CODE_CITY = Option::get(static::$module, "NAMEPROP_CITY");
        $PROP_CODE_STREET = Option::get(static::$module, "NAMEPROP_STREET");
        $PROP_CODE_HOUSE = Option::get(static::$module, "NAMEPROP_HOUSE");
        $PROP_CODE_INDEX = Option::get(static::$module, "NAMEPROP_INDEX");
        foreach($arPropVals["properties"] as $prop){
            if($prop["CODE"] == $PROP_CODE_YDDATA)$YDDataPropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_USERNAME)$NamePropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_MIDLNAME)$MidlNamePropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_LASTNAME)$LastNamePropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_USERPHONE)$PhonePropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_USEREMAIL)$EmailPropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_CITY){ $CityPropID = $prop["ID"]; $CityPropType = $prop["TYPE"];}
            if($prop["CODE"] == $PROP_CODE_STREET)$StreetPropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_HOUSE)$HousePropID = $prop["ID"];
            if($prop["CODE"] == $PROP_CODE_INDEX)$IndexPropID = $prop["ID"];
        }
        if($YDDataPropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($YDDataPropID);
          if($rsProp)$YDDataPropValue = $rsProp->getValue();
        }
        if($NamePropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($NamePropID);
          if($rsProp)$NamePropValue = $rsProp->getValue();
        }
        if($MidlNamePropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($MidlNamePropID);
          if($rsProp)$MidlNamePropValue = $rsProp->getValue();
        }
        if($LastNamePropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($LastNamePropID);
          if($rsProp)$LastNamePropValue = $rsProp->getValue();
        }
        if($PhonePropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($PhonePropID);
          if($rsProp)$PhonePropValue = $rsProp->getValue();
        }
        if($EmailPropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($EmailPropID);
          if($rsProp)$EmailPropValue = $rsProp->getValue();
        }
        if($StreetPropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($StreetPropID);
          if($rsProp)$StreetPropValue = $rsProp->getValue();
        }
        if($HousePropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($HousePropID);
          if($rsProp)$HousePropValue = $rsProp->getValue();
        }
        if($IndexPropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($IndexPropID);
          if($rsProp)$IndexPropValue = $rsProp->getValue();
        }
        if($CityPropID){
          $rsProp = $propertyCollection->getItemByOrderPropertyId($CityPropID);
          if($rsProp)$CityPropValue = $rsProp->getValue();
        }
        if($CityPropValue && $CityPropType == "LOCATION"){
            $arLocs = \Bitrix\Sale\Location\LocationTable::getList([
                'filter' => ['=CODE' => $CityPropValue, '=NAME.LANGUAGE_ID' => LANGUAGE_ID],
                'select' => ['*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE']
                ])->fetch();
            if($arLocs["NAME_RU"])$CityPropValue = $arLocs["NAME_RU"];
        }
        $to_yd_warehouse = "0";
        if(mb_substr_count(mb_strtolower($YDDataPropValue), "����� ������")){
          $to_yd_warehouse = "1";
        }
        $arYDData = [];
        if($YDDataPropValue){
            foreach(explode("|", $YDDataPropValue) as $YField){
                $arField = explode(":",trim($YField));
                $arYDData[trim($arField[0])] = trim($arField[1]);
            }
        }
        $DataOrder = [
            "sender_id" => "51878", // 51878 - oldos-shop.ru, 47371 - outlet.oldos-shop.ru
            "order_num" => $orderId,
            "order_weight" => $BASKET["WEIGHT"]/1000,
            "order_length" => $rDim,
            "order_width" => $rDim,
            "order_height" => $rDim,
            "order_requisite" => $this->options["IDENTITIES"]["requisites"][0]["id"],
            "order_warehouse" => $this->options["IDENTITIES"]["warehouses"][0]["id"],
            "order_comment" => $order->getField("USER_DESCRIPTION"),
            "order_assessed_value" => ($BASKET["PRICE"] + round($order->getDeliveryPrice())),
            "order_amount_prepaid" => $order->isPaid() ? ($BASKET["PRICE"] + round($order->getDeliveryPrice())) : 0,
            "order_delivery_cost" => round($order->getDeliveryPrice()),
            "is_manual_delivery_cost" => "1",
            "recipient" => [
                    "first_name" => $NamePropValue ? $NamePropValue : $arUser["NAME"],
                    "middle_name" => $MidlNamePropValue ? $MidlNamePropValue : $arUser["SECOND_NAME"],
                    "last_name" => $LastNamePropValue ? $LastNamePropValue : $arUser["LAST_NAME"],
                    "phone" => $PhonePropValue ? $PhonePropValue : $arUser["PERSONAL_PHONE"],
                    "email" => $EmailPropValue ? $EmailPropValue : $arUser["EMAIL"]
                ],
            "deliverypoint" => [
                    "city" => $CityPropValue,
                    "street" => $StreetPropValue,
                    "house" => preg_replace('/[^0-9]+/', '-', $HousePropValue),
                    "floor" => "",
                    "housing" => "",
                    "index" => $IndexPropValue
                ],
            "delivery" => [
                    "delivery" => $arYDData["DID"],
                    "direction" => $arYDData["direction"],
                    "tariff" => $arYDData["tariffId"],
                    "pickuppoint" => $arYDData["PPID"],
                    "to_yd_warehouse" => $to_yd_warehouse
                ]
        ];
        $DataOrder["delivery"]["to_yd_warehouse"] = "1";
        foreach($BASKET["ITEMS"] as $BItem){
            $ARTICLE = false;
            $ART_CODE = "ARTNUMBER";
            $arProduct = CIBlockElement::GetList([],["ID"=>$BItem["PRODUCT_ID"]],false,false,["PROPERTY_".$ART_CODE])->GetNext();
            if($arProduct["PROPERTY_".$ART_CODE."_VALUE"])$ARTICLE = $arProduct["PROPERTY_".$ART_CODE."_VALUE"];
            if(!$ARTICLE){
                $mxResult = CCatalogSku::GetProductInfo($BItem["PRODUCT_ID"]);
                if($mxResult["ID"])$arProduct = CIBlockElement::GetList([],["ID"=>$mxResult["ID"]],false,false,["PROPERTY_".$ART_CODE])->GetNext();
                if($arProduct["PROPERTY_".$ART_CODE."_VALUE"])$ARTICLE = $arProduct["PROPERTY_".$ART_CODE."_VALUE"];
            }
            if(!$ARTICLE)$ARTICLE = $BItem["PRODUCT_ID"];
            $DataOrder["order_items"][] = [
                "orderitem_article" => $ARTICLE,
                "orderitem_name" => $BItem["NAME"],
                "orderitem_quantity" => $BItem["QUANTITY"],
                "orderitem_cost" => $BItem["PRICE"],
                "orderitem_vat_value" => $BItem["VAT"],
            ];
        }
        if($DataOrder["delivery"]["pickuppoint"])unset($DataOrder["deliverypoint"]);
        return $DataOrder;
    }

    public function getYDOrderData($orderID){
        if(intval($orderID) && Loader::includeModule("sale")){
            $PROP_CODE_YDDATA = Option::get(static::$module, "NAMEPROP_YDDATA");
            $order = Sale\Order::load(intval($orderID));
            $propertyCollection = $order->getPropertyCollection();
            foreach ($propertyCollection as $propertyItem) {
                if ($propertyItem->getField("CODE") == $PROP_CODE_YDDATA){
                    $YDDataPropValue = $propertyItem->getValue();
                    $arYDData = [];
                    if($YDDataPropValue){
                        foreach(explode("|", $YDDataPropValue) as $YField){
                            $arField = explode(":",trim($YField));
                            $arYDData[trim($arField[0])] = trim($arField[1]);
                        }
                    }
                    return $arYDData;
                }
            }
        }else{
            return false;
        }
    }

    public function deleteOrder($order_id){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'order_id' => $order_id
            ];
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }

    public function updateOrder($order_id, $data){
        $METHOD_NAME = debug_backtrace()[0]["function"];
        $arData = [
                'client_id' => $this->options["IDENTITIES"]["client"]["id"],
                'sender_id' => $this->options["IDENTITIES"]["senders"][0]["id"],
                'order_id' => $order_id
            ];
        foreach($data as $key => $val){
            $arData[$key] = $val;
        }
        $methodKey = $this->options["METHOD_KEYS"][$METHOD_NAME];
        $arData['secret_key'] = md5($this->getPostValues($arData) . $methodKey);
        $res = $this->cURL($this->arUrls[$METHOD_NAME], $arData);
        return json_decode($res["RESULT"],true);
    }

    public function updateYDStatuses(){
        // ��������� ������� �������� �� ���������� �� �������
        if(Loader::includeModule("sale")){
            $arIDS = [];
            $arTOrders = [];
            $fromDate = date("Y-m-d", strtotime("-6 day"));
            $arYOrders = $this->searchSenderOrdersStatuses("", $fromDate);
            foreach($arYOrders["data"]["orders"] as $arYOrder){
                $arNStatus = false;
                foreach($arYOrder["statuses"] as $arYStatus){
                    if($arYStatus["time"] && $arYStatus["uniform_status"]){
                        $time = MakeTimeStamp($arYStatus["time"], "YYYY-MM-DD HH:MI:SS");
                        if(!$arNStatus || $arNStatus["TIME"] < $time){
                            $arNStatus = [
                                "ORDER_ID" => $arYOrder["order_num"],
                                "TIME" => $time,
                                "STATUS" => $arYStatus["uniform_status"]
                            ];
                        }
                    }
                }
                if($arNStatus){
                    $arIDS[] = $arNStatus["ORDER_ID"];
                    $arTOrders[$arNStatus["ORDER_ID"]] = $arNStatus;
                }
            }

            $PROP_CODE_YDDATA = Option::get(static::$module, "NAMEPROP_YDDATA");
            $rsOrders = \Bitrix\Sale\Order::getList([
                'filter' => [
                    'ID' => $arIDS,
                    '=PROPERTY_VAL.CODE' => $PROP_CODE_YDDATA
                ],
                'order' => ['ID' => 'DESC'],
                'select' => ['ID', 'PROPERTY_VAL.VALUE'],
                'runtime' => [
                    new \Bitrix\Main\Entity\ReferenceField(
                        'PROPERTY_VAL',
                        '\Bitrix\sale\Internals\OrderPropsValueTable',
                        ["=this.ID" => "ref.ORDER_ID"],
                        ["join_type"=>"left"]
                    ),
                ]
              ]);
            while($arLOrder = $rsOrders->fetch()){
                if($arTOrders[$arLOrder["ID"]]){
                    $arYDData = [];
                    if($arLOrder["SALE_INTERNALS_ORDER_PROPERTY_VAL_VALUE"]){
                        foreach(explode("|", $arLOrder["SALE_INTERNALS_ORDER_PROPERTY_VAL_VALUE"]) as $YField){
                            $arField = explode(":",trim($YField));
                            $arYDData[trim($arField[0])] = trim($arField[1]);
                        }
                    }
                    if($arTOrders[$arLOrder["ID"]]["STATUS"] && 
                      $arYDData["STATUS"] != $arTOrders[$arLOrder["ID"]]["STATUS"]){
                        $arYDData["STATUS"] = $arTOrders[$arLOrder["ID"]]["STATUS"];
                        $strYDData = "";
                        $i = 0;
                        foreach($arYDData as $ydKey => $ydVal){
                            $i++;
                            if($i>1)$strYDData .= " | ";
                            $strYDData .= $ydKey.":".$ydVal;
                        }
                        $order = Sale\Order::load($arLOrder["ID"]);
                        $propertyCollection = $order->getPropertyCollection();
                        foreach ($propertyCollection as $property){
                            if($property->getField('CODE') == $PROP_CODE_YDDATA){
                                $property->setValue($strYDData);
                            }
                        }
                        $order->save();
                    }
                }
            }
        }
    }

    public static function CheckEventHandler(){
        $INSTALLED = "N";
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $EvList = $eventManager->findEventHandlers("sale", "OnSaleOrderSaved");
        foreach($EvList as $eventOne){
          if($eventOne["TO_MODULE_ID"] == static::$module &&
            $eventOne["TO_METHOD"] == "OnSaleOrderSavedHandler"){
              $INSTALLED = "Y";
            }
        }
        return $INSTALLED;
    }
}