<?
namespace PYDELIVERYEventsHandler;

use PYDELIVERYMain\PYDELIVERYModuleMain,
    Bitrix\Main\Config\Option,
    \Bitrix\Main\Event,
    Bitrix\Main\Loader,
    Bitrix\Sale;

class PYDELIVERYEventHandler{
    
    protected static $module = "pavelbabich.ydelivery";

    public function OnEndBufferContentHandler(&$content)
    {


    }
    
    public function OnSaleComponentOrderOneStepDeliveryHandler(&$arResult, &$arUserResult, &$arParams)
    {
        global $APPLICATION;
        $YD_ID = false;
        $arADelivers = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
        foreach($arADelivers as $deliver){
            if($deliver["CLASS_NAME"] == '\Sale\Handlers\Delivery\YdeliverypHandler'){
                $YD_ID = $deliver["ID"]; break;
            }
        }
        if($YD_ID){
            foreach($arResult["DELIVERY"] as $dKey => $delivery){
                if($delivery["ID"] == $YD_ID){
                    $arResult["DELIVERY"][$dKey]["DESCRIPTION"].= "<br>".$delivery["CALCULATE_DESCRIPTION"];
                    $arResult["DELIVERY"][$dKey]["CALCULATE_DESCRIPTION"] = "";
                }
            }
        }
        global $APPLICATION;
        $APPLICATION->AddHeadScript('https://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU');
        $APPLICATION->AddHeadScript('/bitrix/js/pavelbabich.ydelivery/saleorderajax.js');
        $APPLICATION->SetAdditionalCSS("/bitrix/js/pavelbabich.ydelivery/saleorderajax.css");
    }

    public function OnSaleOrderBeforeSavedHandler(\Bitrix\Main\Event $event){
        // ������� ����� ������������� ������
        $order = $event->getParameter("ENTITY");
        $propertyCollection = $order->getPropertyCollection();
        $shipmentCollection = $order->getShipmentCollection();
        $SHOW_ERROR_PP = Option::get(static::$module, "SHOW_ERROR_PP");
        $PROP_CODE_YDDATA = Option::get(static::$module, "NAMEPROP_YDDATA");
        if($SHOW_ERROR_PP == "Y" && !$order->getId()){
            foreach ($shipmentCollection as $shipment){
                $Delivery = $shipment->getDelivery();
                if(substr_count(get_class($Delivery),"YdeliverypHandler")){
                    $propsData = [];
                    foreach ($propertyCollection as $propertyItem){
                        if (!empty($propertyItem->getField("CODE"))){
                            $propsData[$propertyItem->getField("CODE")] = trim($propertyItem->getValue());
                        }
                    }
                    foreach ($propertyCollection as $propertyItem) {
                        if ($propertyItem->getField("CODE") == $PROP_CODE_YDDATA){
                            $YDDataPropValue = $propertyItem->getValue();
                            $arYDData = [];
                            if($YDDataPropValue){
                                foreach(explode("|", $YDDataPropValue) as $YField){
                                    $arField = explode(":",trim($YField));
                                    $arYDData[trim($arField[0])] = trim($arField[1]);
                                }
                            }
                        }
                    }
                    if(!isset($arYDData["DID"])){
                        return new \Bitrix\Main\EventResult(
                          \Bitrix\Main\EventResult::ERROR,
                          new \Bitrix\Sale\ResultError(GetMessage("PYDELIVERY_MAIN_MODULE_PP_NOT_SELECTED"), 'SALE_EVENT_WRONG_ORDER'),
                          'sale'
                        );
                    }
                }
            }
        }
    }

    public function OnSaleOrderSavedHandler(\Bitrix\Main\Event $event){
        // ������� ���������� ������
        $AUTOEXPORT = Option::get(static::$module, "AUTOEXPORT");
        $EXPORTONLYPAYED = Option::get(static::$module, "EXPORTONLYPAYED");
        if($AUTOEXPORT == "Y" && $EXPORTONLYPAYED != "Y"){
            $order = $event->getParameter("ENTITY");
            $oldValues = $event->getParameter("VALUES");
            $isNew = $event->getParameter("IS_NEW");
            $shipmentCollection = $order->getShipmentCollection();
            $propertyCollection = $order->getPropertyCollection();
            if($isNew){
                foreach ($shipmentCollection as $shipment){
                    $Delivery = $shipment->getDelivery();
                    if(substr_count(get_class($Delivery),"YdeliverypHandler")){
                        // �������� ������ � ������ ��������
                        $YDELIVERY = new PYDELIVERYModuleMain();
                        $YDOrder = $YDELIVERY->GetOrderData($order->getId());
                        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/YDExport.log", date("d-m-Y H:i:s")."; YDOrder=".print_r($YDOrder,1).";\n", FILE_APPEND);
                        $ExportResult = $YDELIVERY->createOrder($YDOrder);
                        if($ExportResult["YDDataPropID"] && $ExportResult["YDDataPropValue"]){
                            $YDDataProp = $propertyCollection->getItemByOrderPropertyId($ExportResult["YDDataPropID"]);
                            $YDDataProp->setValue($ExportResult["YDDataPropValue"]);
                            $YDDataProp->save();
                        }
                        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/YDExport.log", date("d-m-Y H:i:s")."; ExportResult=".print_r($ExportResult,1).";\n", FILE_APPEND);
                        break;
                    }
                }
            }
        }
    }

    public static function OnSalePaymentSetFieldHandler(\Bitrix\Main\Event $event){
        // ������� ������ ������
        $AUTOEXPORT = Option::get(static::$module, "AUTOEXPORT");
        $EXPORTONLYPAYED = Option::get(static::$module, "EXPORTONLYPAYED");
        if(Loader::includeModule("sale")){ // && $AUTOEXPORT == "Y"
            $payment = $event->getParameter('ENTITY');
            $tPayID = $payment->getId();
            $name = $event->getParameter('NAME');
            $value = $event->getParameter('VALUE');
            $old_value = $event->getParameter('OLD_VALUE');
            if($name == "PAID" && $value=="Y" && $old_value=="N"){
                $ORDER_ID = $payment->getField('ORDER_ID');
                $eOrder = Sale\Order::load($ORDER_ID);
                $shipmentCollection = $eOrder->getShipmentCollection();
                $propertyCollection = $eOrder->getPropertyCollection();
                foreach ($shipmentCollection as $shipment){
                    $Delivery = $shipment->getDelivery();
                    if(substr_count(get_class($Delivery),"YdeliverypHandler")){
                        $YDELIVERY = new PYDELIVERYModuleMain();
                        $YDOrder = $YDELIVERY->GetOrderData($eOrder->getId());
                        if($EXPORTONLYPAYED == "Y"){
                          // �������� ������ � ������ ��������
                          $ExportResult = $YDELIVERY->createOrder($YDOrder);
                          if($ExportResult["YDDataPropID"] && $ExportResult["YDDataPropValue"]){
                              $YDDataProp = $propertyCollection->getItemByOrderPropertyId($ExportResult["YDDataPropID"]);
                              $YDDataProp->setValue($ExportResult["YDDataPropValue"]);
                              $YDDataProp->save();
                          }
                        }else{
                          // ��������� ������� ������� ��� ������
                            $arYData = $YDELIVERY->getYDOrderData($ORDER_ID);
                            if($arYData["YDORDER_ID"]){
                                $YDOrder["order_amount_prepaid"] = $YDOrder["order_assessed_value"];
                                $UpdateResult = $YDELIVERY->updateOrder($arYData["YDORDER_ID"], $YDOrder);
                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    public static function CheckEventHandler(){
        $INSTALLED = "N";
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $EvList = $eventManager->findEventHandlers("sale", "OnSalePaymentSetField");
        foreach($EvList as $eventOne){
          if($eventOne["TO_MODULE_ID"] == static::$module &&
            $eventOne["TO_METHOD"]=="OnSalePaymentSetFieldHandler"){
              $INSTALLED = "Y";
            }
        }
        return $INSTALLED;
    }

    public static function CheckBeforeSavedHandler(){
        $INSTALLED = "N";
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $EvList = $eventManager->findEventHandlers("sale", "OnSaleOrderBeforeSaved");
        foreach($EvList as $eventOne){
          if($eventOne["TO_MODULE_ID"] == static::$module &&
            $eventOne["TO_METHOD"]=="OnSaleOrderBeforeSavedHandler"){
              $INSTALLED = "Y";
            }
        }
        return $INSTALLED;
    }
}