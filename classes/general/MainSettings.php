<?
namespace PYDELIVERYSettings;

use Bitrix\Main\Config\Option;

class PYDELIVERYMainSettings
{
    protected static $params = array();
    protected static $module = "pavelbabich.ydelivery";
    public static $settings_name = "PYDELIVERYMAIN";

    public static function GetSiteSetting($key) {
        $value = Option::get(static::$module, $key);
        return $value;
    }

    public static function SaveSettings() {
        $arVals = $_POST[PYDELIVERYMainSettings ::$settings_name];
        if (is_array($arVals)) {
            foreach($arVals as $sName => $sVal) {
                //$TYPE = static::$params[$sName]["TYPE"];
                $TYPE = "TEXT";
                // COption::SetOptionString(static::$module, $sName, $sVal);
                Option::set(static::$module, $sName, $sVal);
            }
        }
    }
}