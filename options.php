<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
define("ADMIN_MODULE_NAME", "pavelbabich.ydelivery");
define("ADMIN_MODULE_ICON", "ICON!");
use PYDELIVERYSettings\PYDELIVERYMainSettings,
    PYDELIVERYMain\PYDELIVERYModuleMain,
    PYDELIVERYEventsHandler\PYDELIVERYEventHandler,
    Bitrix\Sale;
//IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("pavelbabich.ydelivery");
CModule::IncludeModule("sale");
$APPLICATION->SetTitle(GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_PYDELIVERY_SETTINGS"));
$APPLICATION->SetAdditionalCSS("/bitrix/js/pavelbabich.ydelivery/saleorderajax.css");

if (!$USER->IsAdmin()) $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$aTabs = array();
if ($USER->IsAdmin()) {
    $aTabs = array(
        array("DIV" => "ydeliverysettings0", "TAB" => GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_COMMON_INFO"), "ICON"=>"main_user_edit", "TITLE"=>GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_COMMON_INFO_AND_SETTINGS")),
        array("DIV" => "ydeliverysettings1", "TAB" => GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_LOCAL_SETTINGS"), "ICON"=>"main_user_edit", "TITLE"=>GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_LOCAL_SETTINGS")),
        array("DIV" => "ydeliverysettings2", "TAB" => GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_TAB_ORDERS"), "ICON"=>"main_user_edit", "TITLE"=>GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_TAB_ORDERS"))
    );
}
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>
<?
if ($REQUEST_METHOD == "POST" && ($_REQUEST["save"] || $_REQUEST["get_uuid"]) && check_bitrix_sessid()){
    if($_POST["PYDELIVERYMAIN"]["AUTOEXPORT"]!="Y")$_POST["PYDELIVERYMAIN"]["AUTOEXPORT"]="N";
    if($_POST["PYDELIVERYMAIN"]["GROUP_PP"]!="Y")$_POST["PYDELIVERYMAIN"]["GROUP_PP"]="N";
    if($_POST["PYDELIVERYMAIN"]["SHOW_ERROR_PP"]!="Y")$_POST["PYDELIVERYMAIN"]["SHOW_ERROR_PP"]="N";
    $SAVE_ERROR = false;
    if($_REQUEST["PYDELIVERYMAIN"]["OPTIONS_KEY"] || $_REQUEST["PYDELIVERYMAIN"]["METHOD_KEYS"]){
      $YDELIVERY = new PYDELIVERYModuleMain($_REQUEST["PYDELIVERYMAIN"]["METHOD_KEYS"], $_REQUEST["PYDELIVERYMAIN"]["OPTIONS_KEY"]);
      if($YDELIVERY->status == "ERROR"){
          $SAVE_ERROR = GetMessage("PYDELIVERY_MAIN_MODULE_KEY_ERRORS");
      }
    }
    if(!$SAVE_ERROR)PYDELIVERYMainSettings::SaveSettings();
}

$HANDLER_INSTALLED = PYDELIVERYEventHandler::CheckEventHandler();
if($HANDLER_INSTALLED != "Y"){
    $eventManager = \Bitrix\Main\EventManager::getInstance();
    $eventManager->registerEventHandler("sale", "OnSalePaymentSetField", "pavelbabich.ydelivery", "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSalePaymentSetFieldHandler");
}

$BEFORESAVED_INSTALLED = PYDELIVERYEventHandler::CheckBeforeSavedHandler();
if($BEFORESAVED_INSTALLED != "Y"){
    $eventManager = \Bitrix\Main\EventManager::getInstance();
    $eventManager->registerEventHandler("sale", "OnSaleOrderBeforeSaved", "pavelbabich.ydelivery", "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSaleOrderBeforeSavedHandler");
}

require_once ($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
?>
<form method="post" action="<?=$APPLICATION->GetCurPageParam()?>" ENCTYPE="multipart/form-data" name="ydeliverysettings_form">
<?echo bitrix_sessid_post();?>
<?
$tabControl->Begin();

if($USER->IsAdmin()){
    ?>
    <?
    //********************
    // zero tab - about
    //********************
    $tabControl->BeginNextTab();

    ?>
    <tr class="">
        <td colspan="2">
            <?=GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_TAB_COMMON_INFO")?><br>
            <?=GetMessage("PYDELIVERY_MAIN_MODULE_CURL_INFO")?>
            <?if(is_callable('curl_init')){?>
              <b style="color:green;"><?=GetMessage("PYDELIVERY_MAIN_MODULE_CURL_ON")?></b>
            <?}else{?>
              <b style="color:red;"><?=GetMessage("PYDELIVERY_MAIN_MODULE_CURL_OFF")?></b>
            <?}?>
            <br/>
            
            <?
            /*
            $oProps = PYDELIVERYModuleMain::getOrderPropsCodeAssoc();
            echo "oProps=<pre>".print_r($oProps,1)."</pre>";
            
            $oPersons = PYDELIVERYModuleMain::getPersonTypes();
            echo "oProps=<pre>".print_r($oPersons,1)."</pre>";
            */
            ?>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td>
        <?if($_REQUEST["ORDER_ID"])$ID = intval($_REQUEST["ORDER_ID"]);
        if($ID){
            $order = Sale\Order::load($ID);
            $propertyCollection = $order->getPropertyCollection();
            $arPropVals = $propertyCollection->getArray();
            $YDELIVERY = new PYDELIVERYModuleMain();
            if($YDELIVERY->status == "ERROR"){
                echo '<b style="color:red;">'.GetMessage("PYDELIVERY_MAIN_MODULE_KEY_ERRORS").'</b>';
            }else{
                $BASKET = $YDELIVERY->getYDBasket($ID);
                $rDim = ceil(pow($BASKET["VOLUME"],1/3));
                $UserID = $order->getUserId();
                if($UserID){
                    $arUser = CUser::GetByID(CUser::GetID())->Fetch();
                }
                
                if($_REQUEST["ORDER"]){
                    $dataOrder = $_REQUEST["ORDER"];
                }else{
                    $dataOrder = $YDELIVERY->GetOrderData($ID);
                }

                if($_REQUEST["EXPORT"]){
                    $ExportResult = $YDELIVERY->createOrder($dataOrder);
                    if($ExportResult["status"]=="ok"){
                        echo '<h2 style="color:green">'.GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_EXPORT_SUCCESS").'</h2>';
                        // $ConfirmResult = $YDELIVERY->confirmSenderOrders($ID);
                    }
                    // echo "<pre>".print_r($dataOrder,1)."</pre>";
                    // echo "<pre>".print_r($ExportResult,1)."</pre>";
                }
                // $OrderInfo = $YDELIVERY->getOrderInfo("7046069");
                // echo "OrderInfo(".$ID.")=<pre>".print_r($OrderInfo,1)."</pre>";
                ?>
                <h2><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER")?>: <?=$ID?></h2>
                <div class="exported_order" style="border:1px solid #aaa;padding:10px;margin:20px 0;">
                    <h3><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_ALL_PROPS")?></h3>
                    <?foreach($arPropVals["properties"] as $prop){
                        if($prop["VALUE"][0]){
                            echo $prop["NAME"].": ".$prop["VALUE"][0].";<br>";
                        }
                    }?>
                </div>
                <div class="exported_order" style="border:1px solid #aaa;padding:10px;margin:20px 0;">
                    <h3><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_EXPORTED_DATA")?></h3>
                    <form action="<?=$APPLICATION->GetCurPageParam()?>" method="post">
                        <input type="hidden" name="ORDER_ID" value="<?=$ID?>">
                        <input type="hidden" name="ORDER[order_num]" value="<?=$ID?>">
                        <input type="hidden" name="ORDER[order_requisite]" value="<?=htmlspecialchars($dataOrder["order_requisite"])?>">
                        <input type="hidden" name="ORDER[order_warehouse]" value="<?=htmlspecialchars($dataOrder["order_warehouse"])?>">
                        <input type="hidden" name="ORDER[is_manual_delivery_cost]" value="1">
                        <input type="hidden" name="ORDER[delivery][delivery]" value="<?=htmlspecialchars($dataOrder["delivery"]["delivery"])?>">
                        <input type="hidden" name="ORDER[delivery][direction]" value="<?=htmlspecialchars($dataOrder["delivery"]["direction"])?>">
                        <input type="hidden" name="ORDER[delivery][tariff]" value="<?=htmlspecialchars($dataOrder["delivery"]["tariff"])?>">
                        <input type="hidden" name="ORDER[delivery][pickuppoint]" value="<?=htmlspecialchars($dataOrder["delivery"]["pickuppoint"])?>">

                        <input type="submit" name="EXPORT" value="<?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_EXPORT_SUBMIT")?>" style="font-size:18px;color:#A00D0D;">
                        <table style="width:100%;">
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_WEIGHT")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[order_weight]" value="<?=htmlspecialchars($dataOrder["order_weight"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DIMENSIONS")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[order_length]" value="<?=htmlspecialchars($dataOrder["order_length"])?>"> X <input name="ORDER[order_width]" value="<?=htmlspecialchars($dataOrder["order_width"])?>"> X <input name="ORDER[order_height]" value="<?=htmlspecialchars($dataOrder["order_height"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_COMMENT")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[order_comment]" value="<?=htmlspecialchars($dataOrder["order_comment"])?>" size="70">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_PRICE")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[order_assessed_value]" value="<?=htmlspecialchars($dataOrder["order_assessed_value"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_PREPAID")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[order_amount_prepaid]" value="<?=htmlspecialchars($dataOrder["order_amount_prepaid"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_COST")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[order_delivery_cost]" value="<?=htmlspecialchars($dataOrder["order_delivery_cost"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_USER_NAME")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[recipient][first_name]" value="<?=htmlspecialchars($dataOrder["recipient"]["first_name"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_USER_SECOND_NAME")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[recipient][middle_name]" value="<?=htmlspecialchars($dataOrder["recipient"]["middle_name"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_USER_LAST_NAME")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[recipient][last_name]" value="<?=htmlspecialchars($dataOrder["recipient"]["last_name"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_USER_PHONE")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[recipient][phone]" value="<?=htmlspecialchars($dataOrder["recipient"]["phone"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_USER_EMAIL")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[recipient][email]" value="<?=htmlspecialchars($dataOrder["recipient"]["email"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_CITY")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[deliverypoint][city]" value="<?=htmlspecialchars($dataOrder["deliverypoint"]["city"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_STREET")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[deliverypoint][street]" value="<?=htmlspecialchars($dataOrder["deliverypoint"]["street"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_HOUSE")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[deliverypoint][house]" value="<?=htmlspecialchars($dataOrder["deliverypoint"]["house"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_FLOOR")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[deliverypoint][floor]" value="<?=htmlspecialchars($dataOrder["deliverypoint"]["floor"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_HOUSING")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[deliverypoint][housing]" value="<?=htmlspecialchars($dataOrder["deliverypoint"]["housing"])?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DELIVERY_INDEX")?>:</td>
                                <td width="80%">
                                    <input name="ORDER[deliverypoint][index]" value="<?=htmlspecialchars($dataOrder["deliverypoint"]["index"])?>">
                                </td>
                            </tr>
                            
                            <tr>
                                <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_TO_YD_WAREHOUSE")?>:</td>
                                <td width="80%">
                                    <select name="ORDER[delivery][to_yd_warehouse]">
                                        <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                                        <option value="0" <?if($dataOrder["delivery"]["to_yd_warehouse"]===0){?>selected="selected"<?}?>><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_TO_YD_WAREHOUSE_0")?></option>
                                        <option value="1" <?if($dataOrder["delivery"]["to_yd_warehouse"]===1){?>selected="selected"<?}?>><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_TO_YD_WAREHOUSE_1")?></option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_GOODS_DATA")?>
                                    <table>
                                        <tr>
                                            <td><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_GOODS_ARTICLE")?></td>
                                            <td><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_GOODS_NAME")?></td>
                                            <td><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_GOODS_QUANTITY")?></td>
                                            <td><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_GOODS_COST")?></td>
                                            <td><?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_GOODS_VAT")?></td>
                                        </tr>
                                        <?foreach($dataOrder["order_items"] as $key => $Item){?>
                                            <tr>
                                                <td><input name="ORDER[order_items][<?=$key?>][orderitem_article]" value="<?=$Item["orderitem_article"]?>"></td>
                                                <td><input name="ORDER[order_items][<?=$key?>][orderitem_name]" value="<?=$Item["orderitem_name"]?>"></td>
                                                <td><input name="ORDER[order_items][<?=$key?>][orderitem_quantity]" value="<?=$Item["orderitem_quantity"]?>"></td>
                                                <td><input name="ORDER[order_items][<?=$key?>][orderitem_cost]" value="<?=$Item["orderitem_cost"]?>"></td>
                                                <td><input name="ORDER[order_items][<?=$key?>][orderitem_vat_value]" value="<?=$Item["orderitem_vat_value"]?>"></td>
                                            </tr>
                                        <?}?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <input type="submit" name="EXPORT" value="<?=GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_EXPORT_SUBMIT")?>" style="font-size:18px;color:#A00D0D;">
                    </form>
                </div>
            <?}?>    
        <?}?>
        </td>
    </tr>

    <?
    //********************
    // first tab - SETTINGS
    //********************
    $tabControl->BeginNextTab();

    ?>
    <tr class="">
        <td colspan="2">
            <?=GetMessage("PYDELIVERY_MAIN_MODULE_PAGE_TAB_API_KEYS")?>
            <?if($SAVE_ERROR){?><br><br><b style="color:red;"><?=$SAVE_ERROR?></b><?}?>
            <br/><br/></td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_METHOD_KEYS")?>:</td>
        <td width="80%">
            <textarea cols="100" rows="31" name="PYDELIVERYMAIN[METHOD_KEYS]"><?=PYDELIVERYMainSettings::GetSiteSetting("METHOD_KEYS")?></textarea>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_OPTION_VALS")?>:</td>
        <td width="80%">
            <textarea cols="100" rows="23" name="PYDELIVERYMAIN[OPTIONS_KEY]"><?=PYDELIVERYMainSettings::GetSiteSetting("OPTIONS_KEY")?></textarea>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_SHOP_CITY")?>:</td>
        <td width="80%">
            <?$ShopCity = PYDELIVERYMainSettings::GetSiteSetting("SHOP_CITY");?>
            <input name="PYDELIVERYMAIN[SHOP_CITY]" id="SHOP_CITY" value="<?=$ShopCity?$ShopCity:GetMessage("PYDELIVERY_MAIN_MODULE_RAND_CITY")?>" size="50">
            <div class="yd_suggestion_box"></div>
            <script>
                var CityField = document.getElementById("SHOP_CITY");
                CityField.addEventListener('keyup', function(){
                    var cityText = CityField.value;
                    clearTimeout( window.myTimeout );
                    window.myTimeout = setTimeout(function(){ showSuggestion(cityText) }, 600);
                });
                
                function showSuggestion(chars){
                    var suggestionBox = document.querySelector("div.yd_suggestion_box");
                    var req = new XMLHttpRequest();
                    req.open("POST", '/bitrix/js/pavelbabich.ydelivery/suggestion.php', true);
                    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                    req.send("chars="+chars+"&type=locality");
                    req.onreadystatechange = function(){
                        if (this.readyState != 4) return;
                        var regResponse = JSON.parse(this.response);
                        if(regResponse.TYPE == "OK"){
                            if(suggestionBox && regResponse.HTML){
                                suggestionBox.innerHTML = regResponse.HTML;
                                document.querySelectorAll("div.yd_suggestion_box p").forEach(function(sugg){
                                    sugg.addEventListener('click', function(){
                                        suggText = this.innerHTML;
                                        document.getElementById("SHOP_CITY").value = suggText;
                                        document.querySelector("div.yd_suggestion_box").className = "yd_suggestion_box";
                                    });
                                });
                            }
                        }else{
                            if(suggestionBox && regResponse.MESSAGE)suggestionBox.innerHTML = regResponse.MESSAGE;
                        }
                        suggestionBox.className = "yd_suggestion_box active";
                    };
                }
            </script>
        </td>
    </tr>
    <?
    $arProps = [];
    $rsProperties = CSaleOrderProps::GetList(["SORT"=>"ASC"], ['ACTIVE'=>'Y'], false, false, ['ID','NAME','CODE','PERSON_TYPE_ID','TYPE']);
    while($arProp = $rsProperties->GetNext()){
        if(in_array($arProp["TYPE"],["TEXT","LOCATION"])){
            $arProps[$arProp["CODE"]][$arProp["PERSON_TYPE_ID"]] = $arProp;
        }
    }
    ?>
    <tr>
        <td colspan="2">
            <hr>
            <?=GetMessage("PYDELIVERY_MAIN_MODULE_SALEORDER_INFO")?>
        </td>
    </tr>
    
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_YDDATA")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_YDDATA]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_YDDATA");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;
                    $selected = false;
                    if($propCode==$SelectedNameProp || (!$SelectedNameProp && $propCode=="YD_DELIVERY_DATA"))$selected = true;
                    ?>
                    <option value="<?=$propCode?>" <?if($selected){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_CITY")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_CITY]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_CITY");
                foreach($arProps as $propCode => $field){?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_STREET")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_STREET]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_STREET");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_HOUSE")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_HOUSE]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_HOUSE");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_INDEX")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_INDEX]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_INDEX");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_RAND_WEIGHT")?>:</td>
        <td width="80%">
            <?$weight = PYDELIVERYMainSettings::GetSiteSetting("RAND_WEIGHT");?>
            <input name="PYDELIVERYMAIN[RAND_WEIGHT]" value="<?=$weight?$weight:100?>">
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_RAND_DIMENSIONS")?>:</td>
        <td width="80%">
            <?
            $length = PYDELIVERYMainSettings::GetSiteSetting("RAND_LENGTH");
            $width = PYDELIVERYMainSettings::GetSiteSetting("RAND_WIDTH");
            $heigth = PYDELIVERYMainSettings::GetSiteSetting("RAND_HEIGHT");
            ?>
            <input name="PYDELIVERYMAIN[RAND_LENGTH]" value="<?=$length?$length:10?>"> X
            <input name="PYDELIVERYMAIN[RAND_WIDTH]" value="<?=$width?$width:10?>"> X
            <input name="PYDELIVERYMAIN[RAND_HEIGHT]" value="<?=$heigth?$heigth:10?>">
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_GROUP_PP")?>:</td>
        <td width="80%">
            <input name="PYDELIVERYMAIN[GROUP_PP]" value="Y" type="checkbox" <?if(PYDELIVERYMainSettings::GetSiteSetting("GROUP_PP")=="Y"){?>checked="checked"<?}?>>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_SHOW_ERROR_PP")?>:</td>
        <td width="80%">
            <input name="PYDELIVERYMAIN[SHOW_ERROR_PP]" value="Y" type="checkbox" <?if(PYDELIVERYMainSettings::GetSiteSetting("SHOW_ERROR_PP")=="Y"){?>checked="checked"<?}?>>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <hr>
            <?=GetMessage("PYDELIVERY_MAIN_MODULE_EXPORT_INFO")?>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_AUTOEXPORT")?>:</td>
        <td width="80%">
            <input name="PYDELIVERYMAIN[AUTOEXPORT]" value="Y" type="checkbox" <?if(PYDELIVERYMainSettings::GetSiteSetting("AUTOEXPORT")=="Y"){?>checked="checked"<?}?>>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_EXPORTONLYPAYED")?>:</td>
        <td width="80%">
            <input name="PYDELIVERYMAIN[EXPORTONLYPAYED]" value="Y" type="checkbox" <?if(PYDELIVERYMainSettings::GetSiteSetting("EXPORTONLYPAYED")=="Y"){?>checked="checked"<?}?>>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_NAME")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_USER_NAME]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_USER_NAME");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_MIDNAME")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_USER_MIDNAME]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_USER_MIDNAME");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_LASTNAME")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_USER_LASTNAME]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_USER_LASTNAME");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_PHONE")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_USER_PHONE]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_USER_PHONE");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    <tr>
        <td width="20%"><?=GetMessage("PYDELIVERY_MAIN_MODULE_NAMEPROP_USER_EMAIL")?>:</td>
        <td width="80%">
            <select name="PYDELIVERYMAIN[NAMEPROP_USER_EMAIL]">
                <option value=""><?=GetMessage("PYDELIVERY_MAIN_MODULE_NOTSELECTED")?></option>
                <?
                $SelectedNameProp = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_USER_EMAIL");
                foreach($arProps as $propCode => $field){
                    if(current($field)["TYPE"]!="TEXT")continue;?>
                    <option value="<?=$propCode?>" <?if($propCode==$SelectedNameProp){?>selected="selected"<?}?>><?=$propCode?></option>
                <?}?>
            </select>
        </td>
    </tr>
    
    
    <?
    //********************
    // Exported Orders
    //********************
    $tabControl->BeginNextTab();
    ?>
    <tr class="">
        <td colspan="2">
        <?
        $YD_ID = false;
        unset($order);
        $arADelivers = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
        foreach($arADelivers as $deliver){
            if($deliver["CLASS_NAME"] == '\Sale\Handlers\Delivery\YdeliverypHandler'){
                $YD_ID = $deliver["ID"]; break;
            }
        }
        if($YD_ID){
            if($_REQUEST["table_id"]=="tbl_rubric")$APPLICATION->RestartBuffer();
            $sTableID = "tbl_rubric"; // ID �������
            $oSort = new CAdminSorting($sTableID, "ID", "desc");
            $lAdmin = new CAdminList($sTableID, $oSort);
            
            if($_REQUEST["SIZEN_1"])$sizenP = intval($_REQUEST["SIZEN_1"]);
                else $sizenP = 10;
            $rsOrders = CSaleOrder::GetList(["ID"=>"DESC"],["DELIVERY_ID"=>$YD_ID],false,["nPageSize"=>$sizenP],["ID","USER_ID","DATE_INSERT","PRICE","STATUS_ID"]);
            // ����������� ������ � ��������� ������ CAdminResult
            $rsOrders = new CAdminResult($rsOrders, $sTableID);
            // ���������� CDBResult �������������� ������������ ���������.
            $rsOrders->NavStart();
            // �������� ����� ������������� ������� � �������� ������ $lAdmin
            $lAdmin->NavText($rsOrders->GetNavPrint(GetMessage("PYDELIVERY_MAIN_MODULE_ORDERS")));
            
            $lAdmin->AddHeaders(array(
                array("id"=>"ID", "content"=>"ID", "sort"=>"id", "default"=>true),
                array("id"=>"USER_ID", "content"=>GetMessage("PYDELIVERY_MAIN_MODULE_USER"), "sort"=>"name", "default"=>true),
                array("id"=>"DATE_INSERT", "content"=>GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_DATE"), "sort"=>"date_insert", "default"=>true),
                array("id"=>"PRICE", "content"=>GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_SUMM"), "sort"=>"summ", "default"=>true),
                array("id"=>"STATUS_ID", "content"=>GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_STATUS"), "sort"=>"status", "default"  =>true),
                array("id"=>"GOODS", "content"=>GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_GOODS"), "sort"=>"goods", "default"=>true),
                array("id"=>"EXPORT", "content"=>GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_EXPORT"), "sort"=>"export", "default"=>true),
            ));
            
            while($arRes = $rsOrders->NavNext(true, "f_")){
                $row =& $lAdmin->AddRow($f_ID, $arRes);
                
                //USER
                $rsUser = CUser::GetByID($f_USER_ID);
                $arUser = $rsUser->Fetch();
                $row->AddInputField("USER_ID", array("size"=>20));
                $row->AddViewField("USER_ID", '<a href="user_edit.php?ID='.$f_USER_ID.'&lang='.LANG.'">'.$arUser["EMAIL"].'</a>');
                
                //Summ
                $row->AddViewField("PRICE", number_format($f_PRICE, 2,"."," "));
                
                //Goods
                $arBasketItems = array();
                $dbBasketItems = CSaleBasket::GetList(array(), array("ORDER_ID" => $f_ID),false,false,array("PRODUCT_ID", "QUANTITY"));
                while($arItem = $dbBasketItems->Fetch()){
                    $Element = CIBlockElement::GetByID($arItem["PRODUCT_ID"])->GetNext();
                    $arBasketItems[] = $Element['NAME'];
                }
                $row->AddViewField("GOODS", implode("; ", $arBasketItems));
                $row->AddViewField("EXPORT", "<a href='".$APPLICATION->GetCurPageParam("ORDER_ID=".$f_ID,["ORDER_ID"])."'>".GetMessage("PYDELIVERY_MAIN_MODULE_ORDER_EXPORT")."</a>");
                
                //Status
                $arStatus = CSaleStatus::GetByID($f_STATUS_ID);
                $row->AddViewField("STATUS_ID", "(".$f_STATUS_ID.") ".$arStatus["NAME"]);
            }
            
            // ������ �������
            $lAdmin->AddFooter(array(
                array("title"=>GetMessage("PYDELIVERY_MAIN_MODULE_FOOT_ORDERS"), "value"=>8), // ���-�� ���������
                array("counter"=>true, "title"=>GetMessage("PYDELIVERY_MAIN_MODULE_T_HEADER"), "value"=>"0"), // ������� ��������� ���������
            ));
            
            // ������� ������� ������ ���������
            $lAdmin->DisplayList();
            if($_REQUEST["table_id"]=="tbl_rubric")die();
        }?>
        </td>
    </tr>
    <tr class="">
        <td colspan="2">
         </td>
    </tr>
<?
}

$tabControl->Buttons(
    array(
    )
);

?>
<?
$tabControl->End();

if ($ex = $APPLICATION->GetException()) {
    $message = new CAdminMessage(GetMessage("rub_save_error"), $ex);
    echo $message->Show();
}
?>
<?
$tabControl->ShowWarnings("ydeliverysettings_form", $message);
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>