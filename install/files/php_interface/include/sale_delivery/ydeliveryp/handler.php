<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale;
use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;
use \Bitrix\Main\Localization\Loc;
use CSaleOrderProps;
use CSaleLocation;
use Bitrix\Main\Loader;
use Bitrix\Main\Error;
use PYDELIVERYSettings\PYDELIVERYMainSettings;
use PYDELIVERYMain\PYDELIVERYModuleMain;

Loc::loadMessages("/bitrix/modules/pavelbabich.ydelivery/lang/ru/include.php");

class YdeliverypHandler extends Base
{
    public static function getClassTitle()
        {
            $MODULE_ID = "pavelbabich.ydelivery";
            Loader::includeModule($MODULE_ID);
            return GetMessage("PYDELIVERY_MAIN_MODULE_NAME");
        }
        
    public static function getClassDescription()
        {
            $MODULE_ID = "pavelbabich.ydelivery";
            Loader::includeModule($MODULE_ID);
            return GetMessage("PYDELIVERY_MAIN_MODULE_NAME");
        }
        
    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
        {
            $MODULE_ID = "pavelbabich.ydelivery";
            if(Loader::includeModule($MODULE_ID)){
                global $APPLICATION;
                $APPLICATION->AddHeadScript('/bitrix/js/pavelbabich.ydelivery/saleorderajax.js');
                $APPLICATION->SetAdditionalCSS("/bitrix/js/pavelbabich.ydelivery/saleorderajax.css");
                
                $result = new CalculationResult();
                $order = $shipment->getCollection()->getOrder();
                $propertyCollection = $order->getPropertyCollection();
                $arPropVals = $propertyCollection->getArray();
                $locationCode = $propertyCollection->getDeliveryLocation()->getValue();
                $loc = \Bitrix\Sale\Location\LocationTable::getRowById($locationCode);
                $arLocs = CSaleLocation::GetByID($locationCode, LANGUAGE_ID);
                
                $YDSHOP_CITY = PYDELIVERYMainSettings::GetSiteSetting("SHOP_CITY");
                $YD_CODE_CITY = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_CITY");
                $YD_CODE_STREET = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_STREET");
                $YD_CODE_HOUSE = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_HOUSE");
                $YD_CODE_INDEX = PYDELIVERYMainSettings::GetSiteSetting("NAMEPROP_INDEX");
                $YDDATA_CODE = PYDELIVERYMainSettings::GetSiteSetting("YD_DELIVERY_DATA");
                $JSONopt = PYDELIVERYMainSettings::GetSiteSetting("OPTIONS_KEY");
                $JSONkeys = PYDELIVERYMainSettings::GetSiteSetting("METHOD_KEYS");
                $YDELIVERY = new PYDELIVERYModuleMain($JSONkeys, $JSONopt);
                if($YDELIVERY->status == "READY"){
                    if(!$YDDATA_CODE)$YDDATA_CODE = "YD_DELIVERY_DATA";

                    if($YD_CODE_CITY && $YDSHOP_CITY){
                    
                        foreach($arPropVals["properties"] as $prop){
                            if($prop["CODE"] == $YD_CODE_CITY)$CityPropID = $prop["ID"];
                            if($prop["CODE"] == $YD_CODE_STREET)$StreetPropID = $prop["ID"];
                            if($prop["CODE"] == $YD_CODE_HOUSE)$HousePropID = $prop["ID"];
                            if($prop["CODE"] == $YD_CODE_INDEX)$IndexPropID = $prop["ID"];
                            if($prop["CODE"] == $YDDATA_CODE)$YDDataPropID = $prop["ID"];
                        }
                        
                        if($arLocs["CITY_NAME"])$YDCityName = $arLocs["CITY_NAME"];
                        elseif($CityPropID){
                            $YDCityName = $propertyCollection->getItemByOrderPropertyId($CityPropID)->getValue();
                        }
                        if($YDDataPropID){
                            $obYDDataProp = $propertyCollection->getItemByOrderPropertyId($YDDataPropID);
                        }
                        if($IndexPropID){
                            $IndexPropValue = $propertyCollection->getItemByOrderPropertyId($IndexPropID)->getValue();
                        }
                        
                        if($_REQUEST["order"]["YD_delivery_type"])$RType = htmlspecialchars($_REQUEST["order"]["YD_delivery_type"]);
                            else $RType = htmlspecialchars($_REQUEST["YD_delivery_type"]);
                        if($RType=="todoor")$type = "todoor";
                        elseif($RType=="post")$type = "post";
                        else $type = "pickup";
                        if($IndexPropValue)$INDEX = $IndexPropValue;
                            elseif($_REQUEST["order"]["YD_delivery_index"])$INDEX = htmlspecialchars($_REQUEST["order"]["YD_delivery_index"]);
                            else $INDEX = htmlspecialchars($_REQUEST["YD_delivery_index"]);
                        if($_REQUEST["order"]["YD_delivery_id"])$DID = intval($_REQUEST["order"]["YD_delivery_id"]);
                            else $DID = intval($_REQUEST["YD_delivery_id"]);
                        if($_REQUEST["order"]["YD_delivery_pp_id"])$PPID = intval($_REQUEST["order"]["YD_delivery_pp_id"]);
                            else $PPID = intval($_REQUEST["YD_delivery_pp_id"]);

                        if($obYDDataProp){
                            $obYDDataProp->setValue("");
                            if($YDCityName){
                                if($DID){
                                    if(mb_strtolower(LANG_CHARSET)=="windows-1251"){
                                        $delList = $YDELIVERY->searchDeliveryList(iconv('windows-1251','utf-8',$YDCityName), $type, $INDEX);
                                    }else{
                                        $delList = $YDELIVERY->searchDeliveryList($YDCityName, $type, $INDEX);
                                    }

                                    $tempData = [];
                                    foreach($delList["data"] as $key => $tData){
                                        if($tData["delivery"]["id"]){
                                            if($tempData[$tData["delivery"]["id"]]){
                                                if($tempData[$tData["delivery"]["id"]]["PRICE"]>$tData["cost"]){
                                                  unset($delList["data"][ $tempData[$tData["delivery"]["id"]]["KEY"] ]);
                                                }else{
                                                  unset($delList["data"][$key]);
                                                  continue;
                                                }
                                            }
                                            $tempData[$tData["delivery"]["id"]] = [
                                                "KEY" => $key,
                                                "PRICE" => $tData["cost"]
                                            ];
                                        }
                                    }
                                    
                                    if($delList["status"]=="ok"){
                                        foreach($delList["data"] as $dService){
                                            if($type == "todoor" && strtolower($dService["type"])!= "todoor"){
                                                if(strtolower($dService["type"])=="post" && !$dService["pickupPoints"]){
                                                
                                                }else continue;
                                            }
                                            if($type == "pickup" &&
                                                strtolower($dService["type"])!= "pickup" &&
                                                strtolower($dService["type"])!= "post"
                                              )continue;
                                            if($dService["delivery"]["id"] == $DID){
                                                if(mb_strtolower(LANG_CHARSET)=="windows-1251"){
                                                    $dService["direction"] = iconv('utf-8','windows-1251',$dService["direction"]);
                                                    $dService["delivery"]["name"] = iconv('utf-8','windows-1251',$dService["delivery"]["name"]);
                                                }

                                                $PRICE = $dService["costWithRules"];
                                                include($_SERVER["DOCUMENT_ROOT"]."/bitrix/js/".$MODULE_ID."/arr_logo.php");
                                                if($arrLogo[$dService["delivery"]["unique_name"]]){
                                                    $description = '<img src="'.$arrLogo[$dService["delivery"]["unique_name"]]["SRC"].'" style="max-width:80px;float:left;margin-right:10px;"><br>';
                                                }else{
                                                    $description = '<img src="'.$arrLogo["RAND"]["SRC"].'" style="max-width:80px;float:left;margin-right:10px;"><br>';
                                                }
                                                $description .= $dService["delivery"]["name"]."<br>";
                                                if($dService["minDays"]){
                                                    $endDate = $dService["maxDays"];
                                                    if($dService["minDays"] == $endDate){
                                                      $period = $dService["minDays"]." ";
                                                    }else{
                                                      $period = $dService["minDays"]."-".$endDate." ";
                                                    }
                                                    $period .= $endDate%10==1&&$endDate%100!=11?GetMessage("PYDELIVERY_MAIN_MODULE_DAY1"):($endDate%10>=2&&$endDate%10<=4&&($endDate%100<10||$endDate%100>=20)?GetMessage("PYDELIVERY_MAIN_MODULE_DAY2"):GetMessage("PYDELIVERY_MAIN_MODULE_DAY3"));
                                                }
                                                if($PPID && is_array($dService["pickupPoints"])){
                                                    foreach($dService["pickupPoints"] as $pickupPoint){
                                                        if($pickupPoint["id"]==$PPID){
                                                            if(mb_strtolower(LANG_CHARSET)=="windows-1251"){
                                                                $pickupPoint["name"] = iconv('utf-8','windows-1251',$pickupPoint["name"]);
                                                                $pickupPoint["full_address"] = iconv('utf-8','windows-1251',$pickupPoint["full_address"]);
                                                            }
                                                            if($pickupPoint["name"])$description .= GetMessage("PYDELIVERY_MAIN_MODULE_POINT").$pickupPoint["name"].";<br>";
                                                            if($pickupPoint["full_address"])$description .= GetMessage("PYDELIVERY_MAIN_MODULE_ADDRESS").$pickupPoint["full_address"].";<br>";
                                                            if(is_array($pickupPoint["phones"]) && !empty($pickupPoint["phones"])){
                                                                $description .= GetMessage("PYDELIVERY_MAIN_MODULE_PHONE");
                                                                foreach($pickupPoint["phones"] as $phone){
                                                                    $description .= $phone["number"].";<br>";
                                                                }
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                                
                                                $arTypes = ["pickup"=>GetMessage("PYDELIVERY_MAIN_MODULE_TYPE_PICKUP"),"post"=>GetMessage("PYDELIVERY_MAIN_MODULE_TYPE_POST"),"todoor"=>GetMessage("PYDELIVERY_MAIN_MODULE_TYPE_TODOOR")];
                                                $YDPropDescr = $arTypes[$type].":".strip_tags(str_replace(["<br>",":","|"]," ",$description))." | DID:".$DID." | TYPE:".$type;
                                                if($PPID)$YDPropDescr .= " | PPID:".$PPID;
                                                if($dService["direction"])$YDPropDescr .= " | direction:".$dService["direction"];
                                                if($dService["tariffId"])$YDPropDescr .= " | tariffId:".$dService["tariffId"];
                                                $obYDDataProp->setValue($YDPropDescr);
                                                // $description .= "<pre>".print_r($dService,1)."</pre>";
                                                break;
                                            }
                                        }
                                    }
                                    $addPrice = floatval($this->config["MAIN"]["ADD_PRICE"]);
                                    $weight = floatval($shipment->getWeight()) / 1000;
                                    $result->setDeliveryPrice($PRICE + $addPrice);
                                    if($period)$result->setPeriodDescription($period);
                                }else{
                                    $result->addError(new Error(GetMessage("PYDELIVERY_MAIN_MODULE_ERROR_NOT_DELIVERY")));
                                    $result->setDescription(GetMessage("PYDELIVERY_MAIN_MODULE_ERROR_NOT_DELIVERY"));
                                }
                            }else{
                                $result->addError(new Error(GetMessage("PYDELIVERY_MAIN_MODULE_ERROR_NOT_CITY")));
                                $result->setDescription(GetMessage("PYDELIVERY_MAIN_MODULE_ERROR_NOT_CITY"));
                            }

                            $COORDS = $YDELIVERY->getCoords($YDCityName);
                            $description .= '<button type="button" id="yd_delivery" data-ydwidget-open onclick=\'initYDeliveryBox("'.$type.'","'.$YDCityName.'","'.$INDEX.'","'.$COORDS["LAT"].'","'.$COORDS["LNG"].'")\'>'.GetMessage("PYDELIVERY_MAIN_MODULE_SELECT_VAR").'</button>';
                            $description .= '<input type="hidden" name="YD_delivery_type" id="YD_delivery_type" value="'.$type.'">';
                            $description .= '<input type="hidden" name="YD_delivery_index" id="YD_delivery_index" value="'.$INDEX.'">';
                            $description .= '<input type="hidden" name="YD_delivery_id" id="YD_delivery_id" value="'.$DID.'">';
                            $description .= '<input type="hidden" name="YD_delivery_pp_id" id="YD_delivery_pp_id" value="'.$PPID.'">';
                            if($delivery["CALCULATE_ERRORS"])$descr .= '<div class="calculate_errors">'.$delivery["CALCULATE_ERRORS"]."</div>";
                            if($description)$result->setDescription($description);
                            return $result;
                        }else{
                            $result->addError(new Error(GetMessage("PYDELIVERY_MAIN_MODULE_DATAPROP_ERROR")));
                            $result->setDescription(GetMessage("PYDELIVERY_MAIN_MODULE_DATAPROP_ERROR"));
                            return $result;
                        }
                    }else{
                        $result->addError(new Error(GetMessage("PYDELIVERY_MAIN_MODULE_CITY_ERROR")));
                        $result->setDescription(GetMessage("PYDELIVERY_MAIN_MODULE_CITY_ERROR"));
                        return $result;
                    }
                }else{
                    $result->addError(new Error(GetMessage("PYDELIVERY_MAIN_MODULE_KEY_ERRORS")));
                    $result->setDescription(GetMessage("PYDELIVERY_MAIN_MODULE_KEY_ERRORS"));
                    return $result;
                }
            }else{
                throw new \Bitrix\Main\SystemException(GetMessage("PYDELIVERY_MAIN_MODULE_ERROR_MODULE"));
            }
        }
        
    public function isCompatible(\Bitrix\Sale\Shipment $shipment)
        {
            // $calcResult = self::calculateConcrete($shipment);
            // return $calcResult->isSuccess();
            return true;
        }
        
    protected function getConfigStructure()
        {
            $MODULE_ID = "pavelbabich.ydelivery";
            Loader::includeModule($MODULE_ID);
            return array(
                "MAIN" => array(
                    "TITLE" => GetMessage("PYDELIVERY_MAIN_HANDLER_SETTINGS"),
                    "DESCRIPTION" => GetMessage("PYDELIVERY_MAIN_HANDLER_SETTINGS"),"ITEMS" => array(
                        "ADD_PRICE" => array(
                                    "TYPE" => "NUMBER",
                                    "MIN" => 0,
                                    "NAME" => GetMessage("PYDELIVERY_MAIN_ORDER_ADD_PRICE")
                        )
                    )
                )
            );
        }
        
    public function isCalculatePriceImmediately()
        {
            return true;
        }
        
    public static function whetherAdminExtraServicesShow()
        {
            return true;
        }
}
?>