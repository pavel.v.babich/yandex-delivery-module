<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use PYDELIVERYSettings\PYDELIVERYMainSettings;
use PYDELIVERYMain\PYDELIVERYModuleMain;
$MODULE_ID = "pavelbabich.ydelivery";
$CityID = intval($_REQUEST["cityID"]);
if($CityID)$arLocs = CSaleLocation::GetByID($CityID, LANGUAGE_ID);
$chars = htmlspecialchars($_REQUEST["chars"]);
$type = htmlspecialchars($_REQUEST["type"]);
if($type=="locality")$type="locality"; // �����
elseif($type=="street")$type="street"; // �����
elseif($type=="house")$type="house"; // ����� ����
elseif($type=="index")$type="index"; // �������� ������
elseif($type=="goods")$type="goods"; // ������������ ������ �� �����-�����, ���������������� � ������.��������
else $type="address"; // �������� �����
CModule::IncludeModule("sale");
if(CModule::IncludeModule($MODULE_ID) && $chars){
    $YDELIVERY = new PYDELIVERYModuleMain();
    if($YDELIVERY->status == "READY"){
        $suggest = $YDELIVERY->autocomplete($chars, $type); // $arLocs["CITY_NAME"]
        if(is_array($suggest["data"]["suggestions"]) && !empty($suggest["data"]["suggestions"])){
            $arResult["TYPE"] = "OK";
            foreach($suggest["data"]["suggestions"] as $suggest){
                // if(!$suggest["addressBlock"]["index"])continue;
                $arResult["HTML"] .= "<p data-city='".$suggest["addressBlock"]["city"]."' data-index='".$suggest["addressBlock"]["index"]."'>".$suggest["value"]."</p>";
            }
        }else{
            $arResult = ["TYPE"=>"ERROR", "MESSAGE"=>"empty", "sug" => ""];
        }
    }else{
        $arResult = ["TYPE"=>"ERROR", "MESSAGE"=>"empty", "sug" => ""];
    }
}else{
    $arResult = ["TYPE"=>"ERROR", "MESSAGE"=>"uncorrect question"];
}
echo json_encode($arResult);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>