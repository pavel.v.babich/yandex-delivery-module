<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true){
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
}else{
  define("TO_YD_DATA_ARRAY", "Y");
}
$start = microtime(true);
$MODULE_ID = "pavelbabich.ydelivery";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/js/".$MODULE_ID."/arr_logo.php");
use PYDELIVERYSettings\PYDELIVERYMainSettings;
use PYDELIVERYMain\PYDELIVERYModuleMain;
if(!function_exists('plural_form')){
    function plural_form($n, $forms){
      return $n%10==1&&$n%100!=11?$forms[0]:($n%10>=2&&$n%10<=4&&($n%100<10||$n%100>=20)?$forms[1]:$forms[2]);
    }
}
if(CModule::IncludeModule($MODULE_ID) && CModule::IncludeModule("sale")){
    $city = htmlspecialchars($_REQUEST["city"]);
    if($_REQUEST["index"])$INDEX = htmlspecialchars($_REQUEST["index"]);
        else $INDEX = "";
    if($_REQUEST["type"]=="todoor")$type = "todoor";
    elseif($_REQUEST["type"]=="post")$type = "post";
    else $type = "pickup";
    if(mb_strtolower(LANG_CHARSET)=="windows-1251"){
        $arPlurals = [
          iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_DAY")),
          iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_DAY_IN")),
          iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_DAYS"))
        ];
        $arDaysName = [
          "1"=> iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_MONDAY")),
          "2"=> iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_TUESDAY")),
          "3"=> iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_WEDNESDAY")),
          "4"=> iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_THURSDAY")),
          "5"=> iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_FRIDAY")),
          "6"=> iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_SATURDAY")),
          "7"=> iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_SUNDAY"))
        ];
    }else{
        $arPlurals = [
          GetMessage("PYDELIVERY_MAIN_MODULE_DAY"),
          GetMessage("PYDELIVERY_MAIN_MODULE_DAY_IN"),
          GetMessage("PYDELIVERY_MAIN_MODULE_DAYS")
        ];
        $arDaysName = [
            "1"=> GetMessage("PYDELIVERY_MAIN_MODULE_MONDAY"),
            "2"=> GetMessage("PYDELIVERY_MAIN_MODULE_TUESDAY"),
            "3"=> GetMessage("PYDELIVERY_MAIN_MODULE_WEDNESDAY"),
            "4"=> GetMessage("PYDELIVERY_MAIN_MODULE_THURSDAY"),
            "5"=> GetMessage("PYDELIVERY_MAIN_MODULE_FRIDAY"),
            "6"=> GetMessage("PYDELIVERY_MAIN_MODULE_SATURDAY"),
            "7"=> GetMessage("PYDELIVERY_MAIN_MODULE_SUNDAY"),
        ];
    }
    $YDELIVERY = new PYDELIVERYModuleMain();
    $AddPrice = 0;
    $DelServices = Bitrix\Sale\Delivery\Services\Manager::getActiveList();
    foreach($DelServices as $DS){
      if($DS["CLASS_NAME"] == '\Sale\Handlers\Delivery\YdeliverypHandler' && $DS["CONFIG"]["MAIN"]["ADD_PRICE"])$AddPrice = $DS["CONFIG"]["MAIN"]["ADD_PRICE"];
    }
    if($city){
        $delList = $YDELIVERY->searchDeliveryList($city, $type, $INDEX);
        // echo "<pre>".print_r($delList,1)."</pre>"; die();
        if($delList["status"]=="ok"){
            $tempData = [];
            foreach($delList["data"] as $key => $tData){
                if($tData["delivery"]["id"]){
                    if($tempData[strtolower($tData["type"])][$tData["delivery"]["id"]]){
                        if($tempData[strtolower($tData["type"])][$tData["delivery"]["id"]]["PRICE"]>$tData["cost"]){
                          unset($delList["data"][ $tempData[strtolower($tData["type"])][$tData["delivery"]["id"]]["KEY"] ]);
                        }else{
                          unset($delList["data"][$key]);
                          continue;
                        }
                    }
                    $tempData[strtolower($tData["type"])][$tData["delivery"]["id"]] = [
                        "KEY" => $key,
                        "PRICE" => $tData["cost"]
                    ];
                }
            }
            if($type == "todoor"){
                foreach($delList["data"] as $dService){
                    if(strtolower($dService["type"])!="todoor"){
                      if(strtolower($dService["type"])!="post" || $dService["pickupPoints"])continue;
                    }
                    if($dService["minDays"] == $dService["maxDays"]){
                      $term = $dService["minDays"];
                    }else{
                      $term = $dService["minDays"]."-".$dService["maxDays"];
                    }
                    $YDPoint = [
                            "TYPE" => $type,
                            "CITY" => $city,
                            "INDEX" => $INDEX,
                            "DELIVERY_ID" => $dService["delivery"]["id"],
                            "DELIVERY_NAME" => $dService["delivery"]["name"],
                            "COST" => $dService["costWithRules"] + $AddPrice,
                            "DATES" => $term,
                            "LOGO" => $arrLogo[$dService["delivery"]["unique_name"]]["PNG"]?$arrLogo[$dService["delivery"]["unique_name"]]["PNG"]:$arrLogo["RAND"]["PNG"]
                        ];
                    ob_start();?>
                    <div class='yDeliveryVar' id='yDeliveryVar_<?=$dService["delivery"]["id"]?>'>
                        <?if($arrLogo[$dService["delivery"]["unique_name"]]){?>
                            <img src="<?=$arrLogo[$dService["delivery"]["unique_name"]]["SRC"]?>">
                        <?}else{?>
                            <img src="<?=$arrLogo["RAND"]["SRC"]?>">
                        <?}?>
                        <div class='yDeliveryVar_NAME'>
                            <span class='yDeliveryVar_PRICE'><?=$dService["costWithRules"]+$AddPrice?><?//=$dService["cost"]?>&#8381;</span>
                            <?=$dService["delivery"]["name"]?>
                            <?if($dService["tariffName"]){?> <?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_TARIF")):GetMessage("PYDELIVERY_MAIN_MODULE_TARIF")?>: <?=$dService["tariffName"]?><?}?>
                        </div>
                        <?
                        if($dService["minDays"] == $dService["maxDays"]){
                          $term = $dService["maxDays"];
                        }else{
                          $term = $dService["minDays"]."-".$dService["maxDays"];
                        }
                        ?>
                        <div class='yDeliveryVar_DATES'><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_LIMIT")):GetMessage("PYDELIVERY_MAIN_MODULE_LIMIT")?>: <?=$term?> <?=plural_form($dService["maxDays"], $arPlurals)?></div>
                        <?if($dService["delivery_date"]){?>
                            <div class='yDeliveryVar_DELIVERY_DATES'><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_DELIVERY_DAYS")):GetMessage("PYDELIVERY_MAIN_MODULE_DELIVERY_DAYS")?>: <?=implode(", ",$dService["delivery_date"])?></div>
                        <?}?>
                        <?if(is_array($dService["delivery"]["courier"]["schedules"]) && !empty($dService["delivery"]["courier"]["schedules"])){?>
                            <div class='yDeliveryVar_SHEDULE'>
                                <?$arShedule = [];
                                foreach($dService["delivery"]["courier"]["schedules"] as $day){
                                    $arShedule[$day["day"]][] = substr($day["from"],0,5)."-".substr($day["to"],0,5);
                                }?>
                                <table>
                                    <tr><td><?=implode("</td><td>",$arDaysName)?></td></tr>
                                    <tr><?//=$arDaysName[$day["day"]]?><?
                                        for($i = 1; $i <= 7; $i++){
                                            if($arShedule[$i]){
                                                ?><td><span><?=implode(",</span><span>",$arShedule[$i])?></span></td><?
                                            }else{
                                                ?><td class="sh_holliday"></td><?
                                            }
                                        }
                                    ?></tr>
                                </table>
                            </div>
                        <?}?>
                        <div class="YD_var_button">
                            <span class="select_YD_var" onclick='selectYDvar(<?=json_encode($YDPoint)?>)'><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_SELECT")):GetMessage("PYDELIVERY_MAIN_MODULE_SELECT")?></span>
                        </div>
                    </div>
                    <?
                    $YDPoint["POPUP_HTML"] = trim(ob_get_clean());
                    $arYDData["TODOOR"][$dService["delivery"]["id"]] = $YDPoint;
                }
                
                ob_start();
                foreach($delList["data"] as $dService){
                    if(strtolower($dService["type"])!="todoor"){
                      if(strtolower($dService["type"])!="post" || $dService["pickupPoints"])continue;
                    }
                    ?>
                    <div class='yDeliveryVar' onclick="openYDTODOORpopup(<?=$dService["delivery"]["id"]?>)" data-id='yDeliveryVar_<?=$dService["delivery"]["id"]?>'>
                        <?if($arrLogo[$dService["delivery"]["unique_name"]]){?>
                            <img src="<?=$arrLogo[$dService["delivery"]["unique_name"]]["SRC"]?>">
                        <?}else{?>
                            <img src="<?=$arrLogo["RAND"]["SRC"]?>">
                        <?}?>
                        <div class='yDeliveryVar_NAME'>
                            <span class='yDeliveryVar_PRICE'><?=$dService["costWithRules"]+$AddPrice?><?//=$dService["cost"]?>&#8381;</span>
                            <?=$dService["delivery"]["name"]?>
                        </div>
                        <?
                        if($dService["minDays"] == $dService["maxDays"]){
                          $term = $dService["maxDays"];
                        }else{
                          $term = $dService["minDays"]."-".$dService["maxDays"];
                        }
                        ?>
                        <div class='yDeliveryVar_DATES'><?=$term?> <?=plural_form($dService["maxDays"], $arPlurals)?></div>
                    </div>
                <?}
                $arYDData["HTML"] = trim(ob_get_clean());
                $arYDData["TYPE"] = "OK";
            }elseif($type == "post_old"){
                
                echo "<pre>".print_r($delList,1)."</pre>";
                die();
                
            }elseif($type == "pickup" || $type == "post"){
                foreach($delList["data"] as $dService){
                    if(strtolower($dService["type"])!=$type &&
                      strtolower($dService["type"])!="post")continue;
                    foreach($dService["pickupPoints"] as $pickupPoint){
                        $YDPoint = [
                                "TYPE" => $type,
                                "CITY" => $city,
                                "INDEX" => $INDEX,
                                "DELIVERY_ID" => $dService["delivery"]["id"],
                                "PP_ID" => $pickupPoint["id"],
                                "DELIVERY_NAME" => $dService["delivery"]["name"],
                                "PP_NAME" => $pickupPoint["name"],
                                "COST" => $dService["costWithRules"]+$AddPrice,
                                "ADDRESS" => $pickupPoint["address"],
                                "FULL_ADDRESS" => $pickupPoint["full_address"],
                                "LAT" => $pickupPoint["lat"],
                                "LNG" => $pickupPoint["lng"],
                                "LOGO" => $arrLogo[$dService["delivery"]["unique_name"]]["PNG"]?$arrLogo[$dService["delivery"]["unique_name"]]["PNG"]:$arrLogo["RAND"]["PNG"]
                            ];                        
                        ob_start();?>
                        <div class='yDeliveryVar' id='yDeliveryVar_<?=$dService["delivery"]["id"]?>'>
                            <?if($arrLogo[$dService["delivery"]["unique_name"]]){?>
                                <img src="<?=$arrLogo[$dService["delivery"]["unique_name"]]["SRC"]?>">
                            <?}else{?>
                                <img src="<?=$arrLogo["RAND"]["SRC"]?>">
                            <?}?>
                            <div class='yDeliveryVar_NAME'>
                                <span class='yDeliveryVar_PRICE'><?=$dService["costWithRules"]+$AddPrice?><?//=$dService["cost"]?>&#8381;</span>
                                <?=$dService["delivery"]["name"]?>
                                <?if($dService["tariffName"]){?> <?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_TARIF")):GetMessage("PYDELIVERY_MAIN_MODULE_TARIF")?>: <?=$dService["tariffName"]?><?}?>
                            </div>
                            <div class='yDeliveryVar_PP_NAME' id='yDelivery_PP_<?=$pickupPoint["id"]?>'><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_POINT")):GetMessage("PYDELIVERY_MAIN_MODULE_POINT")?>: <?=$pickupPoint["name"]?></div>
                            <?
                            if($dService["minDays"] == $dService["maxDays"]){
                              $term = $dService["maxDays"];
                            }else{
                              $term = $dService["minDays"]."-".$dService["maxDays"];
                            }
                            ?>
                            <div class='yDeliveryVar_DATES'><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_LIMIT")):GetMessage("PYDELIVERY_MAIN_MODULE_LIMIT")?>: <?=$term?> <?=plural_form($dService["maxDays"], $arPlurals)?></div>
                            <div class='yDeliveryVar_PP_ADDRESS'><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_ADDRESS")):GetMessage("PYDELIVERY_MAIN_MODULE_ADDRESS")?>: <?=$pickupPoint["full_address"]?></div>
                            <?if(is_array($pickupPoint["phones"]) && !empty($pickupPoint["phones"])){?>
                                <div class='yDeliveryVar_PP_PHONE'>
                                    <?foreach($pickupPoint["phones"] as $phone){?>
                                        <span><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_PHONE")):GetMessage("PYDELIVERY_MAIN_MODULE_PHONE")?><?=$phone["number"]?></span>
                                    <?}?>
                                </div>
                            <?}?>
                            <?if(is_array($pickupPoint["schedules"]) && !empty($pickupPoint["schedules"])){?>
                                <div class='yDeliveryVar_SHEDULE'>
                                    <?$arShedule = [];
                                    foreach($pickupPoint["schedules"] as $day){
                                        $arShedule[$day["day"]][] = substr($day["from"],0,5)."-".substr($day["to"],0,5);
                                    }?>
                                    <table>
                                        <tr><td><?=implode("</td><td>",$arDaysName)?></td></tr>
                                        <tr><?//=$arDaysName[$day["day"]]?><?
                                            for($i = 1; $i <= 7; $i++){
                                                if($arShedule[$i]){
                                                    ?><td><span><?=implode(",</span><span>",$arShedule[$i])?></span></td><?
                                                }else{
                                                    ?><td class="sh_holliday"></td><?
                                                }
                                            }
                                        ?></tr>
                                    </table>
                                </div>
                            <?}?>
                            <?if(false && is_array($dService["delivery_date"]) && !empty($dService["delivery_date"])){?>
                                <div class='yDeliveryVar_PP_DELIVERY_DATE'>
                                    <?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_POSSIBLE_DATES")):GetMessage("PYDELIVERY_MAIN_MODULE_POSSIBLE_DATES")?>
                                    <?foreach($dService["delivery_date"] as $dd){?>
                                        <span><?=$dd?></span>
                                    <?}?>
                                </div>
                            <?}?>
                            <div class="YD_var_button">
                                <span class="select_YD_var" onclick='selectYDvar(<?=json_encode($YDPoint)?>)'><?=(mb_strtolower(LANG_CHARSET)=="windows-1251")?iconv('windows-1251','utf-8',GetMessage("PYDELIVERY_MAIN_MODULE_SELECT")):GetMessage("PYDELIVERY_MAIN_MODULE_SELECT")?></span>
                            </div>
                        </div>
                        <?
                        $YDPoint["BALOON_HTML"] = trim(ob_get_clean());
                        $arYDData["PP"][] = $YDPoint;
                    }
                }

                /* === GROUPING === */
                if(false && count($arYDData["PP"])>100){
                    $Div = 5;
                    $arYDData["PP_COUNT"] = count($arYDData["PP"]);
                    $arYDData["MIN_LAT"] = 90;
                    $arYDData["MIN_LNG"] = 180;
                    foreach($arYDData["PP"] as $pp){
                        if($pp["LAT"] > $arYDData["MAX_LAT"])$arYDData["MAX_LAT"] = $pp["LAT"];
                        if($pp["LNG"] > $arYDData["MAX_LNG"])$arYDData["MAX_LNG"] = $pp["LNG"];
                        if($pp["LAT"] < $arYDData["MIN_LAT"])$arYDData["MIN_LAT"] = $pp["LAT"];
                        if($pp["LNG"] < $arYDData["MIN_LNG"])$arYDData["MIN_LNG"] = $pp["LNG"];
                    }
                    $latKoef = round(($arYDData["MAX_LAT"] - $arYDData["MIN_LAT"]) / $Div, 6);
                    $lngKoef = round(($arYDData["MAX_LNG"] - $arYDData["MIN_LNG"]) / $Div, 6);

                    $GROUPS = [];
                    for($i=0; $i <= $Div*$Div-1; $i++)
                        $GROUPS[] = [];
                    foreach($arYDData["PP"] as $pKey => $pp){
                      $latY = $pp["LAT"] - $arYDData["MIN_LAT"];
                      $lngX = $pp["LNG"] - $arYDData["MIN_LNG"];
                      $lvlY = floor($latY / $latKoef);
                      $lvlX = floor($lngX / $lngKoef);
                      if($lvlY == $Div)$lvlY--;
                      if($lvlX == $Div)$lvlX--;
                      $GNUM = $lvlY * $Div + $lvlX;
                      $GROUPS[$GNUM]["LAT"] = $arYDData["MIN_LAT"] + (0.5 + $lvlY) * $latKoef;
                      $GROUPS[$GNUM]["LNG"] = $arYDData["MIN_LNG"] + (0.5 + $lvlX) * $lngKoef;
                      $GROUPS[$GNUM]["PP"][] = $pKey;
                    }
                    $arYDData["GROUPS"] = $GROUPS;
                }
                /* === GROUPING === */
                
                ob_start();
                foreach($delList["data"] as $dService){
                    if(strtolower($dService["type"])!=$type &&
                        strtolower($dService["type"])!="post"
                      )continue;
                    foreach($dService["pickupPoints"] as $pickupPoint){?>
                        <div class='yDeliveryVar' onclick="openYDBaloon(<?=$pickupPoint["id"]?>)" data-id='yDeliveryVar_<?=$dService["delivery"]["id"]?>'>
                            <?if($arrLogo[$dService["delivery"]["unique_name"]]){?>
                                <img src="<?=$arrLogo[$dService["delivery"]["unique_name"]]["SRC"]?>">
                            <?}else{?>
                                <img src="<?=$arrLogo["RAND"]["SRC"]?>">
                            <?}?>
                            <div class='yDeliveryVar_NAME'>
                                <span class='yDeliveryVar_PRICE'><?=$dService["costWithRules"]+$AddPrice?><?//=$dService["cost"]?>&#8381;</span>
                                <?=$dService["delivery"]["name"]?>
                            </div>
                            <div class='yDeliveryVar_PP_NAME' id='yDelivery_PP_<?=$pickupPoint["id"]?>'><?=$pickupPoint["name"]?></div>
                            <?if($dService["minDays"] == $dService["maxDays"]){
                              $term = $dService["maxDays"];
                            }else{
                              $term = $dService["minDays"]."-".$dService["maxDays"];
                            }?>
                            <div class='yDeliveryVar_DATES'><?=$term?> <?=plural_form($dService["maxDays"], $arPlurals)?></div>
                        </div>
                    <?}
                }
                $arYDData["HTML"] = trim(ob_get_clean());
                $arYDData["TYPE"] = "OK";
                // echo "<div class='yDeliveryVar'><pre>".print_r($dService,1)."</pre></div>";
            }
        }elseif($delList["error"]){
            $ERROR = $delList["error"];
            foreach($delList["data"]["errors"] as $err){
                $ERROR .="<br>".$err;
            }
            $arYDData = ["TYPE"=>"ERROR","MESSAGE"=>"ERROR: ".$ERROR];
        }else{
            $arYDData = ["TYPE"=>"ERROR","MESSAGE"=>"ERROR: unknown unswer"."<pre>".print_r($delList,1)."</pre>"];
        }
    }else{
        $arYDData = ["TYPE"=>"ERROR","MESSAGE"=>"ERROR: no city in post"];
    }
}else{
    $arYDData = ["TYPE"=>"ERROR","MESSAGE"=>"ERROR: ydelivery module not installed"];
}
$arYDData["WORKING_TIME"] = 'Working time: '.round(microtime(true) - $start, 4).' sec.';
if(TO_YD_DATA_ARRAY != "Y"){
  echo json_encode($arYDData);
  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
}