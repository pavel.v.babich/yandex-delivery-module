<?
$arrLogo = [
    'CDEK' => [
        'SRC' => '/upload/pavelbabich.ydelivery/cdek.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/cdek64x64.png'
    ],
    'Beta_Post' => [
        'SRC' => '/upload/pavelbabich.ydelivery/rp.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/rp64x64.png'
    ],
    'Beta_Post_Online' => [
        'SRC' => '/upload/pavelbabich.ydelivery/rp.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/rp64x64.png'
    ],
    'PickPoint' => [
        'SRC' => '/upload/pavelbabich.ydelivery/pickpoint.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/pp64x64.png'
    ],
    'PEK' => [
        'SRC' => '/upload/pavelbabich.ydelivery/pek.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/pek64x64.png'
    ],
    'CCCB' => [
        'SRC' => '/upload/pavelbabich.ydelivery/cccb.png',
        'PNG' => '/upload/pavelbabich.ydelivery/cccb64x64.png'
    ],
    'Boxberry' => [
        'SRC' => '/upload/pavelbabich.ydelivery/boxberry.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/boxberry64x64.png'
    ],
    'MaxiPost' => [
        'SRC' => '/upload/pavelbabich.ydelivery/MaxiPost.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/MaxiPost64x64.png'
    ],
    'Strizh' => [
        'SRC' => '/upload/pavelbabich.ydelivery/strizh.png',
        'PNG' => '/upload/pavelbabich.ydelivery/strizh64x64.png'
    ],
    'RAND' => [
        'SRC' => '/upload/pavelbabich.ydelivery/delivery.svg',
        'PNG' => '/upload/pavelbabich.ydelivery/delivery64x64.png'
    ]
];
?>