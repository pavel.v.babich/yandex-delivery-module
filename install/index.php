<?
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__, 'ru');
if(class_exists("pavelbabich_ydelivery")) return;

Class pavelbabich_ydelivery extends CModule
{
    var $MODULE_ID = "pavelbabich.ydelivery";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function pavelbabich_ydelivery()
    {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __DIR__);
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = GetMessage("PYDELIVERY_MAIN_MODULE_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("PYDELIVERY_MAIN_MODULE_INSTALL_DESCR");
        $this->PARTNER_NAME = "Pavel Babich";
        $this->PARTNER_URI = "https://vk.com/skif_p";
    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);
        CopyDirFiles(__DIR__ ."/files/js/pavelbabich.ydelivery", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js/pavelbabich.ydelivery", true);
        CopyDirFiles(__DIR__ ."/files/php_interface/include/sale_delivery/ydeliveryp", $_SERVER["DOCUMENT_ROOT"]."/bitrix/php_interface/include/sale_delivery/ydeliveryp", true);
        CopyDirFiles(__DIR__ ."/files/upload/pavelbabich.ydelivery", $_SERVER["DOCUMENT_ROOT"]."/upload/pavelbabich.ydelivery", true);
        
        if(Loader::includeModule("sale")){
            $oProps = new CSaleOrderProps;
            $rsPersons = CSalePersonType::GetList([], []);
            while ($arPerson = $rsPersons->GetNext()){
                $rsProp = CSaleOrderProps::GetList(["SORT"=>"ASC"], ["PERSON_TYPE_ID"=>$arPerson["ID"],"CODE"=>"YD_DELIVERY_DATA"],false,false,[]);
                if($arProp = $rsProp->Fetch()){ //�������� ��� ����������!
                
                }else{
                    $rsPropsGroup = CSaleOrderPropsGroup::GetList(["SORT"=>"ASC"],["PERSON_TYPE_ID"=>$arPerson["ID"]],false,false,[]);
                    while($arPropsGroup = $rsPropsGroup->Fetch())
                    {
                        $arFields = array(
                           "PERSON_TYPE_ID" => $arPerson["ID"],
                           "PROPS_GROUP_ID" => $arPropsGroup["ID"],
                           "NAME" => "Yandex Delivery data",
                           "CODE" => "YD_DELIVERY_DATA",
                           "TYPE" => "TEXT",
                           "DEFAULT_VALUE" => "",
                           "REQUIED" => "N",
                           "SORT" => 1000,
                           "USER_PROPS" => "N",
                           "UTIL" => "Y",
                           "IS_LOCATION" => "N",
                           "IS_LOCATION4TAX" => "N",
                           "DESCRIPTION" => "",
                           "IS_EMAIL" => "N",
                           "IS_PROFILE_NAME" => "N",
                           "IS_PAYER" => "N",
                           "SIZE1" => 0,
                           "SIZE2" => 0,
                        );
                        $ID = $oProps->Add($arFields);
                       break;
                    }
                }
            }
        }
        
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler("main","OnEndBufferContent", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnEndBufferContentHandler");
        $eventManager->registerEventHandler("sale","OnSaleOrderSaved", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSaleOrderSavedHandler");
        $eventManager->registerEventHandler("sale","OnSaleOrderBeforeSaved", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSaleOrderBeforeSavedHandler");
        $eventManager->registerEventHandler("sale","OnSalePaymentSetField", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSalePaymentSetFieldHandler");
        $eventManager->registerEventHandler("sale","OnSaleComponentOrderOneStepDelivery", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSaleComponentOrderOneStepDeliveryHandler");
        return true;
    }

    function DoUninstall()
    {
        UnRegisterModule($this->MODULE_ID);
        DeleteDirFilesEx("/bitrix/js/pavelbabich.ydelivery");
        DeleteDirFilesEx("/bitrix/php_interface/include/sale_delivery/ydeliveryp");
        DeleteDirFilesEx("/upload/pavelbabich.ydelivery");
        
        UnRegisterModuleDependences("main", "OnEndBufferContent", $this->MODULE_ID, "PYDELIVERYEventHandler", "OnEndBufferContentHandler");
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepDelivery", $this->MODULE_ID, "PYDELIVERYEventHandler", "OnSaleComponentOrderOneStepDeliveryHandler");        
        $eventManager = \Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler("main","OnEndBufferContent", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnEndBufferContentHandler");
        $eventManager->unRegisterEventHandler("sale","OnSaleOrderSaved", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSaleOrderSavedHandler");
        $eventManager->unRegisterEventHandler("sale","OnSaleOrderBeforeSaved", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSaleOrderBeforeSavedHandler");
        $eventManager->unRegisterEventHandler("sale","OnSalePaymentSetField", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSalePaymentSetFieldHandler");
        $eventManager->unRegisterEventHandler("sale","OnSaleComponentOrderOneStepDelivery", $this->MODULE_ID, "PYDELIVERYEventsHandler\\PYDELIVERYEventHandler", "OnSaleComponentOrderOneStepDeliveryHandler");
        return true;
    }
}
?>